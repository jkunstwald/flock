in vec3 aPosition;
in vec2 aTexCoord;
in vec3 aNormal;
in vec3 aTangent;
in vec3 aBitangent;

// -- instancing data --
in vec4 aModelC0;
in vec4 aModelC1;
in vec4 aModelC2;
in vec4 aModelC3;

in vec4 aPrevModelC0;
in vec4 aPrevModelC1;
in vec4 aPrevModelC2;
in vec4 aPrevModelC3;

in uint aMaterialId;

out VS_OUT {
	vec2 texCoord;
	vec3 normal;
	vec3 tangent;

	vec3 fragPosWS; // World Space
	vec4 fragPosVS; // View Space
	vec4 fragPosSS; // Screen Space

	vec3 tangentCamPos;
	vec3 tangentFragPos;

	vec4 HDCPosition;
	vec4 prevHDCPosition;
} vOut;

out vec3 vPosition;
out vec3 vNormal;
out vec4 vScreenPosition;

uniform mat4 uView;
uniform mat4 uProj;

uniform vec3 uCamPos;

// Used to calculate motion vectors in the fragment shader
uniform mat4 uCleanVp;
uniform mat4 uPrevCleanVp;

void main()
{
    mat4 model = mat4(aModelC0, aModelC1, aModelC2, aModelC3);
    mat4 prevModel = mat4(aPrevModelC0, aPrevModelC1, aPrevModelC2, aPrevModelC3);

    vOut.texCoord 	= aTexCoord;
    vOut.normal 	= normalize(mat3(model) * aNormal);
    vOut.tangent 	= normalize(mat3(model) * aTangent);

    vec4 worldSpace = model * vec4(aPosition, 1.0);
    vOut.fragPosVS 	= uView * worldSpace;
    //vOut.fragPosSS 	= uProj * vOut.fragPosVS; // this should not differ from the line below
    vOut.fragPosSS 	= uProj * uView * model * vec4(aPosition, 1.0);
    vOut.fragPosWS 	= vec3(worldSpace);

    vec3 T   = vOut.tangent;
    vec3 N   = vOut.normal;
    vec3 B   = normalize(cross(T, N));
    mat3 TBN = transpose(mat3(T, B, N));

    vOut.tangentCamPos  = TBN * uCamPos;
    vOut.tangentFragPos  = TBN * vOut.fragPosWS;

    vOut.HDCPosition 		= uCleanVp * model * vec4(aPosition, 1.0);
    vOut.prevHDCPosition 	= uPrevCleanVp * prevModel * vec4(aPosition, 1.0);
	
    gl_Position = vOut.fragPosSS;
}
