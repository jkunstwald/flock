#glow pipeline opaque

#include <glow-pipeline/pass/opaque/opaquePass.glsl>

#include <glow-material/material-ggx.glsl>

uniform vec3 uCamPos;

uniform float uMetallic;
uniform float uRoughness;
uniform vec3 uAlbedo;

uniform bool uShowClusterHeatmap;
uniform bool uShowClusterIndexHash;

uniform vec3 uSunDirection;
uniform vec3 uSunColor;
uniform float uAmbientIntensity;

in VS_OUT {
	vec2 texCoord;
	vec3 normal;
	vec3 tangent;

	vec3 fragPosWS; // World Space
	vec4 fragPosVS; // View Space
	vec4 fragPosSS; // Screen Space

	vec3 tangentCamPos;
	vec3 tangentFragPos;

	vec4 HDCPosition;
	vec4 prevHDCPosition;
} vIn;

// -- Debug Output Helpers --

float fade(float low, float high, float value){
    float mid = (low+high)*0.5;
    float range = (high-low)*0.5;
    float x = 1.0 - clamp(abs(mid-value)/range, 0.0, 1.0);
    return smoothstep(0.0, 1.0, x);
}

vec3 getHeatmapColor(float intensity){
    vec3 blue = vec3(0.0, 0.0, 0.15);
    vec3 cyan = vec3(0.0, 0.35, 0.35);
    vec3 green = vec3(0.0, .65, 0.0);
    vec3 yellow = vec3(1.0, 0.9, 0.0);
    vec3 red = vec3(1.0, 0.0, 0.0);

    vec3 color = (
        fade(-0.25, 0.25, intensity)*blue +
        fade(0.0, 0.5, intensity)*cyan +
        fade(0.25, 0.75, intensity)*green +
        fade(0.5, 1.0, intensity)*yellow +
        smoothstep(0.75, 1.0, intensity)*red
    );
    return color;
}

void main() {
    vec3 cameraDistanceVector = uCamPos - vIn.fragPosWS;
    vec3 N = normalize(vIn.normal);
    vec3 V = normalize(cameraDistanceVector);
    float cameraDistanceSquared = dot(cameraDistanceVector, cameraDistanceVector);

    vec3 color = vec3(0, 0, 0);

    // IBL
    color += uAmbientIntensity * iblSpecularGGX(N, V, uAlbedo, uRoughness, uMetallic);

    // Sunlight
    float shadowAmount = getOPCFShadowVisibility(vIn.fragPosWS, cameraDistanceSquared);
    color += (1 - shadowAmount) * uSunColor * shadingGGX(N, V, uSunDirection, uAlbedo, uRoughness, uMetallic);

    int lightAmount = 0; // debug variable

    // Tube Lights
    FOREACH_LIGHT(vIn.fragPosVS.z, i) 
    {
        LightData light = getLight(i);

        vec3 L;
        vec3 radiance;
        getTubeLightInfo(light, vIn.fragPosWS, V, N, uRoughness, L, radiance);

        color += radiance * shadingGGX(N, V, L, uAlbedo, uRoughness, uMetallic);

        ++lightAmount; // debug variable
    }

    // -- Debug Output --

    if (uShowClusterIndexHash)
    {
        const ivec3 hashMod = ivec3(2^32);
        const ivec3 hashMul = ivec3(2654435761);

        ivec3 hashedCluster = ivec3(mod(ivec3(getCurrentCluster() * hashMul), hashMod));
        vec3 normalizedHashCluster = vec3(hashedCluster) / vec3(hashMod);

        color = clamp(vec3(normalizedHashCluster.z) + normalizedHashCluster * .25, vec3(-1), vec3(1));
    }
    else if (uShowClusterHeatmap)
    {
        color = getHeatmapColor(lightAmount / 25.0);
    }

    // -- /Debug Output --

    // AO + Pipeline Output
    outputOpaqueGeometry(applyAo(color), getFragmentVelocity(vIn.HDCPosition, vIn.prevHDCPosition));
}
