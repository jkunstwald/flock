#glow pipeline transparent

#include <glow-pipeline/pass/transparent/oitPass.glsl>

uniform sampler2D uTexture;

in vec2 gsTexCoord;
in vec4 gsTint;

void main()
{
    vec3 particleColor = gsTint.rgb;
    float particleAlpha = texture2D(uTexture,gsTexCoord).r * gsTint.a;

    //float oitAlpha = getOitMaterialAlpha(vec3(0,1,0), normalize(vec3(0,1,0)), vec3(0.1)) * particleAlpha;
    outputOitGeometry(gl_FragCoord.z, particleColor, particleAlpha);
}
