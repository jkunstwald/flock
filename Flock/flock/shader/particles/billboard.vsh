in vec3   aPosition;
in vec3   aVelocity;
in vec3   aColor;
in float  aLifetime;

out vec3 vsVelocity;
out vec4 vsTint;

uniform float uParticleLifetime;

void main()
{
    gl_Position = vec4(aPosition, 1.0);
    vsTint.a   = pow(sin(3.1415*(aLifetime/uParticleLifetime)),2.f);
    vsTint.rgb = aColor;
    vsVelocity = aVelocity;
}


