in vec3   aPosition;
in vec3   aVelocity;
in vec3   aColor;
in float  aLifetime;

void main()
{
    gl_Position = vec4(aPosition, 1.0);
}


