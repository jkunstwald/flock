layout(points)             in;
layout(points)             out;
layout(max_vertices = 120) out;

in vec3           vs_Color[];
in vec3           vs_Position[];
in vec3           vs_Velocity[];

out vec3          gsPosition;
out vec3          gsVelocity;
out vec3          gsColor;
out float         gsLifetime;

uniform float uTime;
uniform float uDeltaTime;
uniform int uEmitCount;
uniform sampler2D uRandomTexture;


//------------------------------------------------------------------------------
//misc stuff
vec3 getRandomVector(float a)
{
    return texture(uRandomTexture,vec2(a,0.5f)).xyz - vec3(0.5f);
}

float getRandom(float a)
{
    return texture(uRandomTexture,vec2(a,0.5f)).x;
}

uint packColor(vec3 c)
{
    uvec3 i = uvec3(c);
    return i.r|(i.g<<8)|(i.b<<16);
}

uint packTypeColor(float type,vec3 color)
{
    return (packColor(color)<<8)|uint(type);
}


//------------------------------------------------------------------------------
//
void emitParticles(int num)
{
    float seed = (uTime*123525.0 + gl_PrimitiveIDIn*1111.f)/1234.f;
    for (int i = 0; i < 100 && i < num; i++) // Hardcoded limitation required
    {
        vec3 dir = normalize(getRandomVector(seed));

        gsColor = vs_Color[0];
        gsVelocity = dir - vs_Velocity[0] * 0.3f;// / (10.0 + getRandom(seed) * 20.0) * 0.3f;
        gsPosition = vs_Position[0];
        gsLifetime = 0;

        EmitVertex();
        EndPrimitive();

        seed += 4207.56;
    }
}


void main()
{
    emitParticles(uEmitCount);
}

