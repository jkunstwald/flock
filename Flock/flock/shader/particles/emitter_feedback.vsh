in vec3   aColor;
in vec3   aPosition;
in vec3   aVelocity;

out vec3  vs_Color;
out vec3  vs_Position;
out vec3  vs_Velocity;

void main()
{
    vs_Color    = aColor;
    vs_Position = aPosition;
    vs_Velocity = aVelocity;
}
