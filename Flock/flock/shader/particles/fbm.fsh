in vec3 vs_Pos;
out vec4 out_FragColor;

//------------------------------------------------------------------------------
//FBM noise stuff
//
//www.shadertoy.com

// matrix to rotate the noise octaves
mat3 m = mat3( 0.00,  0.80,  0.60,
              -0.80,  0.36, -0.48,
              -0.60, -0.48,  0.64 );

float hash( float n )
{
    return fract(sin(n)*43758.5453);
}


float noise( in vec3 x )
{
    vec3 p = floor(x);
    vec3 f = fract(x);

    f = f*f*(3.0-2.0*f);

    float n = p.x + p.y*57.0 + 113.0*p.z;

    float res = mix(mix(mix( hash(n+  0.0), hash(n+  1.0),f.x),
                        mix( hash(n+ 57.0), hash(n+ 58.0),f.x),f.y),
                    mix(mix( hash(n+113.0), hash(n+114.0),f.x),
                        mix( hash(n+170.0), hash(n+171.0),f.x),f.y),f.z);
    return res;
}

float fbmX( vec3 p )
{
    float f;
    f = 0.5000*noise( p ); p = m*p*2.02;
    f += 0.2500*noise( p ); p = m*p*1.03;
    f += 0.1250*noise( p ); p = m*p*2.01;
    f += 0.0625*noise( p );
    return f;
}

float fbmY( vec3 p )
{
    float f;
    f = 0.5000*noise( p ); p = m*p*1.2;
    f += 0.2500*noise( p ); p = m*p*2.03;
    f += 0.1250*noise( p ); p = m*p*2.1;
    f += 0.0625*noise( p );
    return f;
}

float fbmZ( vec3 p )
{
    float f;
    f = 0.5000*noise( p ); p = m*p*1.04;
    f += 0.2500*noise( p ); p = m*p*1.07;
    f += 0.1250*noise( p ); p = m*p*2.1;
    f += 0.0625*noise( p );
    return f;
}

vec3 fbm( vec3 p )
{
    vec3 z = vec3(fbmX(p),fbmY(p),fbmZ(p));
//    z = vec3(fbmX(p + z.x*2.0),fbmY(p + z.y*2.0),fbmZ(p + z.z*2.0));
//    z = vec3(fbmX(p + z.x*4.0),fbmY(p + z.y*4.0),fbmZ(p + z.z*4.0));
    return normalize(z) - vec3(0.5);
}


void main()
{
   out_FragColor.rgb = fbm(vs_Pos*8.0);
}
