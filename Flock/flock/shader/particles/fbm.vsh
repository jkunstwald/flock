uniform float u_Layer;

in vec3 attr_position;
out vec3 vs_Pos;

void main()
{
    gl_Position = vec4(2.0*attr_position - vec3(1.0,1.0,0.0),1.0);
    vs_Pos = vec3(gl_Position.xy,u_Layer*2.0 - 1.0);
}


