layout(points)             in;
layout(points)             out;
layout(max_vertices = 1)   out;

in vec3           vs_Color[];
in float          vs_Age[];
in vec3           vs_Position[];
in vec3           vs_Velocity[];

out vec3          gsPosition;
out vec3          gsVelocity;
out vec3          gsColor;
out float         gsLifetime;

uniform float     uParticleLifetime;


//------------------------------------------------------------------------------
//
void main()
{
    if (vs_Age[0] < uParticleLifetime)
    {
        // Only re-emit particles within lifetime limit
        gsColor = vs_Color[0];
        gsVelocity = vs_Velocity[0];
        gsPosition = vs_Position[0];
        gsLifetime = vs_Age[0];

        EmitVertex();
        EndPrimitive();
    }
}

