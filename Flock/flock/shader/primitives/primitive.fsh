#glow pipeline transparent

#include <glow-pipeline/pass/transparent/oitPass.glsl>

uniform vec4 uColor;

void main()
{
    outputOitGeometry(gl_FragCoord.z, uColor.rgb * uColor.a, uColor.a);
}
