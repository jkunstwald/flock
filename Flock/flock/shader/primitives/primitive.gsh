layout(lines)            in;
layout(triangle_strip)   out;
layout(max_vertices = 4) out;

uniform mat4  uMVP;
uniform mat4  uMV;
uniform vec3  uRight;
uniform vec3  uUp;
uniform float uBillboardSize;

void simpleBillboard()
{
    vec3 pos     = gl_in[0].gl_Position.xyz;
    vec3 up      = uUp * uBillboardSize;
    vec3 right   = uRight * uBillboardSize;

    pos         -= right;
    gl_Position  = uMVP * vec4(pos, 1.0);
    //gsTexCoord  = vec2(0.0, 0.0);
    EmitVertex();

    pos         += up;
    gl_Position  = uMVP * vec4(pos, 1.0);
    //gsTexCoord  = vec2(0.0, 1.0);
    EmitVertex();

    pos         -= up;
    pos         += right;
    gl_Position  = uMVP * vec4(pos, 1.0);
    //gsTexCoord  = vec2(1.0, 0.0);
    EmitVertex();

    pos         += up;
    gl_Position  = uMVP * vec4(pos, 1.0);
    //gsTexCoord  = vec2(1.0, 1.0);
    EmitVertex();

    EndPrimitive();
}


//void velocityAwareBillboard()
//{
//    mat3 MV = mat3(uMV);

//    //screen-space velocity
//    vec3 v      = MV*vsVelocity[0];
//    v.z         = 0.0;
//    float len   = length(v);
//    float scale = min(len*uVelocityScale*10.0,10.0);
//    vec3 v0     = v/len;

//    //velocity basis from screen-space velocity vector
//    vec3 v1     = vec3(-v0.y,v0.x,0);
//    vec3 xaxis  = v0*MV;
//    vec3 yaxis  = v1*MV;
//    vec3 zaxis  = cross(xaxis,yaxis);
//    mat3 basis  = mat3(xaxis*scale,yaxis,zaxis);

//    float w = uBillboardSize;
//    float h = uBillboardSize;
//    vec3 pos = gl_in[0].gl_Position.xyz;
//    gsTint = vsTint[0];

//    //top right point
//    vec3 topright = basis*vec3(w,h,0);
//    gl_Position   = uMVP*vec4(pos + topright,1);
//    gsTexCoord   = vec2(0,0);
//    EmitVertex();

//    //top left point
//    vec3 topleft = basis*vec3(-w,h,0);
//    gl_Position = uMVP*vec4(pos + topleft,1);
//    gsTexCoord = vec2(1,0);
//    EmitVertex();

//    //bottom right point
//    vec3 bottomleft = basis*vec3(w,-h,0);
//    gl_Position = uMVP*vec4(pos + bottomleft,1);
//    gsTexCoord = vec2(0,1);
//    EmitVertex();

//    //bottom left point
//    vec3 bottomright = basis*vec3(-w,-h,0);
//    gl_Position = uMVP*vec4(pos + bottomright,1);
//    gsTexCoord = vec2(1,1);
//    EmitVertex();

//    EndPrimitive();
//}

void main()
{
    simpleBillboard();
}
