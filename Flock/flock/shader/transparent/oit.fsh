#glow pipeline transparent

#include <glow-pipeline/pass/transparent/oitPass.glsl>

uniform vec3 uCamPos;

uniform float uMetallic;
uniform float uRoughness;
uniform float uMaterialAlpha;
uniform vec3 uAlbedo;

uniform mat4 uProj;
uniform mat4 uView;

in VS_OUT {
	vec2 texCoord;
	vec3 normal;
	vec3 tangent;

	vec3 fragPosWS; // World Space
	vec4 fragPosVS; // View Space
	vec4 fragPosSS; // Screen Space

	vec3 tangentCamPos;
	vec3 tangentFragPos;

	vec4 HDCPosition;
	vec4 prevHDCPosition;
} vIn;

void main()
{
    OITFragmentInfo fragInfo;
    fragInfo.N = vIn.normal;
    fragInfo.V = normalize(uCamPos - vIn.fragPosWS);
    fragInfo.R = reflect(-fragInfo.V, fragInfo.N);
    fragInfo.posWorldSpace = vIn.fragPosWS;
    fragInfo.posViewSpace = vIn.fragPosVS;
    fragInfo.posScreenSpace = vIn.fragPosSS;
    fragInfo.proj = uProj;
    fragInfo.view = uView;

    OITMaterial material;
    material.albedo = uAlbedo;
    material.roughness = uRoughness;
    material.specular = vec3(.25);
    material.alpha = getOitMaterialAlpha(fragInfo.N, fragInfo.V, material.specular) * uMaterialAlpha;
    material.reflectivity = 0.5;

    OITResult result;
    getOitResult(material, fragInfo, result);
    outputOitGeometry(gl_FragCoord.z, result.color, material.alpha, result.offset);
}
