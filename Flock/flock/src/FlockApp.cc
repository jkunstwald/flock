#include "FlockApp.hh"

#include <cassert>
#include <cmath>
#include <functional>
#include <memory>
#include <vector>

#include <imgui/imgui.h>

#include <glm/ext.hpp>

#include <GLFW/glfw3.h>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>
#include <glow/data/TextureData.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/TextureCubeMap.hh>
#include <glow/objects/VertexArray.hh>
#include <glow/util/DefaultShaderParser.hh>

#include <glow-extras/assimp/Importer.hh>
#include <glow-extras/camera/legacy/GenericCamera.hh>
#include <glow-extras/colors/color.hh>
#include <glow-extras/geometry/Cube.hh>
#include <glow-extras/geometry/Quad.hh>
#include <glow-extras/material/IBL.hh>
#include <glow-extras/pipeline/RenderScene.hh>
#include <glow-extras/pipeline/lights/Light.hh>
#include <glow-extras/pipeline/stages/StageCamera.hh>
#include <glow-extras/pipeline/stages/implementations/OpaqueStage.hh>
#include <glow-extras/pipeline/stages/implementations/OutputStage.hh>

#include "logic/LightC.hh"
#include "logic/MaterialC.hh"
#include "logic/MeshC.hh"
#include "logic/TransformC.hh"
#include "particles/Emitter.hh"
#include "swarm/SwarmlingC.hh"
#include "util/FileSaver.hh"

#ifdef FLOCK_EMBED_SHADERS
#include "../generated/flock_shaders.hh"
#endif

using namespace glow;
using namespace glow::camera;
using namespace glow::pipeline;

#define RUN_GAME true
#define USE_MONKEY_MESH true
#define GO_FULLSCREEN false

namespace flock
{
void FlockApp::startupGameCore()
{
    mCameraController = std::make_unique<FollowCameraController>(getCamera());
    mInstancingManager = std::make_shared<InstancingManager>();
    mPrimitiveParticleRenderer = std::make_unique<PrimitiveParticleRenderer>(&mRegistry);

    mParticleSystem = std::make_unique<ParticleSystem>();
    mParticleSystem->init();

    mPhysicsEngine.init(mRegistry);
}

void FlockApp::loadLevel()
{
    auto const cubeMesh = glow::geometry::Cube<>().generate();
    auto const pillarMesh = glow::geometry::Cube<>().generate();
    auto const pillarMat = MaterialC{glm::vec3(.35f), .4f, .35f};

    //    auto const levelMat = MaterialC{glm::vec3(.65f), .5f, .75f};
    //    auto const levelFloor = -300;
    //    auto const levelSizeX = 400;
    //    auto const levelSizeY = 400;

    //    auto const floorEnt = mRegistry.create();
    //    mRegistry.assign<TransformC>(floorEnt, glow::transform(glm::vec3(0, levelFloor, 0), {}, glm::vec3(levelSizeX, 1, levelSizeY)));
    //    mRegistry.assign<InstancedMeshC>(floorEnt, mInstancingManager->createInstance(cubeMesh, levelMat, true));

    auto const pillarDistance = 30.f;
    auto const pillarBorderStart = 10;
    auto const pillarBorderEnd = 7;
    auto const pillarSize = glm::vec3(1.5f, 5.f, 1.5f);

    for (int x = -pillarBorderStart; x < pillarBorderStart; ++x)
        for (int y = -pillarBorderStart; y < pillarBorderStart; ++y)
        {
            if (x >= -pillarBorderEnd && x < pillarBorderEnd && y >= -pillarBorderEnd && y < pillarBorderEnd)
            {
                continue;
            }

            {
                auto const hash = static_cast<int>(x ^ (y << 8)) % 32;
                auto const hashFloat = hash / 32.f;

                auto borderRank = 0;

                if (x == pillarBorderStart - 1 || x == -pillarBorderStart || y == pillarBorderStart - 1 || y == -pillarBorderStart)
                    borderRank = 2;
                else if (x == pillarBorderStart - 2 || x == -pillarBorderStart + 1 || y == pillarBorderStart - 2
                         || y == -pillarBorderStart + 1)
                    borderRank = 1;

                auto const pillarPos = glm::vec3(x * pillarDistance, -20 + hashFloat * 7.5f + borderRank * 15.f, y * pillarDistance);
                auto const pillarEnt = mRegistry.create();
                mRegistry.assign<TransformC>(pillarEnt, glow::transform(pillarPos, glm::quat{}, pillarSize));
                mRegistry.assign<InstancedMeshC>(pillarEnt, mInstancingManager->createInstance(pillarMesh, pillarMat, true));
            }
        }
}

void FlockApp::startupGameLogic()
{
    loadLevel();

    mGameEvents = std::make_unique<GameEvents>();
    mSwarm = std::make_unique<Swarm>(&mRegistry);
    mHive = std::make_unique<Hive>(&mRegistry, &mPhysicsEngine, mInstancingManager, mGameEvents.get(), mMeshMonkey,
                                   mMeshIcosphere, mMeshTerrorSquid);
    mPlayerController = std::make_unique<PlayerController>(&mRegistry, mHive.get(), mGameEvents.get(), mMeshPlane);
    mChalice = std::make_unique<Chalice>(&mRegistry, mPlayerController.get(), mGameEvents.get(), mMeshBlood);
    mAnimationController = std::make_unique<AnimationController>(&mRegistry);
    mGamePhase = std::make_unique<GameState>(getPipelineScene().get(), mSwarm.get());
    mGameController = std::make_unique<GameController>(mGamePhase.get(), mGameEvents.get());

    mHive->setChalice(mChalice.get());
}
void FlockApp::teardownGameLogic()
{
    mRegistry.reset();

    mGameController.reset();
    mGamePhase.reset();
    mAnimationController.reset();
    mChalice.reset();
    mPlayerController.reset();
    mHive.reset();
    mSwarm.reset();
    mGameEvents.reset();
}

void FlockApp::drawOpaqueGeometry(UsedProgram &shader, const CameraData &camData, bool zPreOnly)
{
    // Non-instanced
    if (zPreOnly)
    {
        mRegistry.view<TransformC, MeshC>().each([&](auto const, TransformC &transform, MeshC &mesh) {
            shader.setUniform("uModel", transform.transform.getModelMatrix());
            mesh.vao->bind().draw();
        });
    }
    else
    {
        mRegistry.view<TransformC, MeshC, MaterialC>().each([&](auto const, TransformC &transform, MeshC &mesh, MaterialC &mat) {
            auto const &model = transform.transform.getModelMatrix();

            shader.setUniform("uModel", model);

            auto const cleanMvp = camData.cleanVp * model;
            auto const prevCleanMvp = camData.prevCleanVp * transform.previousModelMatrix;
            shader.setUniform("uCleanMvp", cleanMvp);
            shader.setUniform("uPrevCleanMvp", prevCleanMvp);

            shader.setUniform("uRoughness", mat.roughness);
            shader.setUniform("uMetallic", mat.metallic);
            shader.setUniform("uAlbedo", mat.albedo);

            mesh.vao->bind().draw();

            transform.previousModelMatrix = model;
        });
    }
}

void FlockApp::internalUpdate(float dt, float realDt)
{
    // Player movement
    {
        glm::vec3 playerMoveDelta{0};
        bool boosting = isKeyPressed(GLFW_KEY_LEFT_SHIFT);
        bool firing = isMouseButtonPressed(GLFW_MOUSE_BUTTON_LEFT);
        float turnControl = 0.f;

        // Lateral
        if (isKeyPressed(GLFW_KEY_A))
            --playerMoveDelta.x;
        if (isKeyPressed(GLFW_KEY_D))
            ++playerMoveDelta.x;

        // Vertical
        if (isKeyPressed(GLFW_KEY_E))
            --playerMoveDelta.y;
        if (isKeyPressed(GLFW_KEY_Q))
            ++playerMoveDelta.y;

        // Longitudinal
        if (isKeyPressed(GLFW_KEY_W))
            --playerMoveDelta.z;
        if (isKeyPressed(GLFW_KEY_S))
            ++playerMoveDelta.z;

        // Controller handling
        if (glfwJoystickPresent(GLFW_JOYSTICK_1))
        {
            int axesCount;
            auto axisVals = glfwGetJoystickAxes(0, &axesCount);

            static float const deadzone = 0.25f;

            float correctedAxes[6];
            for (auto i = 0u; i < 6; ++i)
            {
                correctedAxes[i] = (std::abs(axisVals[i]) < deadzone ? 0 : axisVals[i]);
            }

            playerMoveDelta.x = std::clamp(playerMoveDelta.x + correctedAxes[0], -1.f, 1.f);
            playerMoveDelta.y = std::clamp(playerMoveDelta.y - correctedAxes[3], -1.f, 1.f);
            playerMoveDelta.z = std::clamp(playerMoveDelta.z + correctedAxes[1], -1.f, 1.f);

            // Right Trigger
            if (correctedAxes[5] >= 0)
                firing = true;

            // Left Trigger
            if (correctedAxes[4] >= 0)
                boosting = true;

            turnControl = correctedAxes[2];
        }

        mPlayerController->update(dt, {playerMoveDelta, firing, boosting, turnControl}, mPhysicsEngine);

        getCamera()->handle.setLookAt(mPlayerController->getTargetCamPos(), mPlayerController->getTargetCamFocus());

        mCameraController->setTargetPosition(mPlayerController->getTargetCamPos());
        mCameraController->setTargetRotation(glow::transform::RotationFromDirection(
            mPlayerController->getTargetCamFocus() - mPlayerController->getTargetCamPos()));

        mCameraController->setCamShakeIntensity(mPlayerController->getCamShakeIntensity());
        mCameraController->update(dt);
    }


#if RUN_GAME
    if (!mPlayerDead)
    {
        mGameController->update(dt, realDt);

        mChalice->update(dt);
        mGamePhase->update(dt);
        mHive->tick(dt, mPlayerController->getPosition());
    }
#endif

    {
        mGameEvents->emitEvents();

        mSwarm->tick(dt);
        mAnimationController->tick(dt);

        mInstancingManager->update(mRegistry);
        mParticleSystem->update(dt, mRegistry);
        mParticleSystem->earlyRender();
        mPrimitiveParticleRenderer->update(dt);
    }
}

void FlockApp::init()
{
    // -- GlfwApp Init --
    {
        setUsePipeline(true);

        setUseDefaultCameraHandling(false);
        setGui(GlfwApp::Gui::ImGui);
        setVSync(true);
        setCursorMode(glfw::CursorMode::Disabled);
        setUpdateRate(static_cast<double>(ticksPerSecond));
        setUsePipelineConfigGui(false);
        setTitle("Flock");

        GlfwApp::init();

#if GO_FULLSCREEN
        toggleFullscreen();

        // -- Clear once before startup --
        {
            GLOW_SCOPED(clearColor, glm::vec3(20, 32, 43) / 255.f);
            glClear(GL_COLOR_BUFFER_BIT);
            glfwSwapBuffers(window());
        }
#endif
    }

    // -- Shader Include Setup --
    {
        // Material Shader Init
        glow::material::IBL::GlobalInit();
    }

    // -- Sample-specific configuration --
    {
        getPipelineScene()->sunIntensity = 3.f;
        getPipelineScene()->atmoScatterIntensity = .25f;
        getPipelineScene()->contrast = 1.15f;

        getPipelineScene()->contrast = 1.6f;
        getPipelineScene()->exposure = 1.5f;
        getPipelineScene()->bloomIntensity = 0.3f;

        // getPipelineScene()->sunDirection = glm::normalize(glm::vec3(0.75f,1,-0.5f));
        getCamera()->setFarPlane(800);
        getCamera()->setPosition({0, -5, 0});
    }

    // -- Asset & Shader loading --
    {
#ifdef FLOCK_EMBED_SHADERS
        for (auto &virtualPair : internal_embedded_files::flock_shaders)
            DefaultShaderParser::addVirtualFile(virtualPair.first, virtualPair.second);
#else
        DefaultShaderParser::addIncludePath("shader");
#endif

        mShaderDepthPre = Program::createFromFiles({"depthPre/geometry.vsh", "depthPre/depthPre.fsh"});
        mShaderDepthPreInstanced = Program::createFromFiles({"instancedGeometry.vsh", "depthPre/depthPre.fsh"});
        mShaderOpaqueForward = Program::createFromFiles({"geometry.vsh", "opaque/litOpaque.fsh"});
        mShaderOpaqueForwardInstanced = Program::createFromFiles({"instancedGeometry.vsh", "opaque/litOpaque.fsh"});
        mShaderTransparent = Program::createFromFiles({"geometry.vsh", "transparent/oit.fsh"});
        mShaderTransparentInstanced = Program::createFromFiles({"instancedGeometry.vsh", "transparent/oit.fsh"});

        const std::string resourcePath = "res/";
#if USE_MONKEY_MESH
        mMeshMonkey = assimp::Importer().load(resourcePath + "suzanne_lowpoly.obj");
#else
        mMeshMonkey = glow::geometry::Cube<>().generate();
#endif
        mMeshIcosphere = assimp::Importer().load(resourcePath + "icosphere.obj");
        mMeshTerrorSquid = assimp::Importer().load(resourcePath + "icosphere.obj");
        mMeshBlood = assimp::Importer().load(resourcePath + "icosphere.obj");
        mMeshPlane = assimp::Importer().load(resourcePath + "fighter.obj");
        auto const skyboxPath = resourcePath + "ibl/studio/skybox/skybox_";
        mSkyboxMap = TextureCubeMap::createFromData(TextureData::createFromFileCube(
            skyboxPath + "posx.hdr", skyboxPath + "negx.hdr", skyboxPath + "posy.hdr", skyboxPath + "negy.hdr",
            skyboxPath + "posz.hdr", skyboxPath + "negz.hdr", ColorSpace::sRGB));
    }

    // -- IBL setup --
    {
        mEnvironmentMap = material::IBL::createEnvMapGGX(mSkyboxMap, 1024);

        auto usedShader = mShaderOpaqueForward->use();
        material::IBL::initShaderGGX(usedShader);
    }

    // -- Savegame loading
    {
        auto const savegame = FileSaver::loadSavegame();
        mHighscore = savegame.highscore;
    }

    // -- Startup --
    {
        startupGameCore();
        startupGameLogic();
    }
}

void FlockApp::update(float)
{
    // mPhysicsEngine.step();

    if (!mPlayerController->isPlayerAlive())
    {
        if (!mPlayerDead)
        {
            mHighscore = std::max(mHighscore, mPlayerController->mScore);
            mPlayerDead = true;
        }
    }
}

void FlockApp::render(float dt)
{
    if (!mPaused)
    {
        mPhysicsEngine.syncSensors(mRegistry);

        auto const gameSpeedDt = dt * mGameController->getTimeScale();
        internalUpdate(gameSpeedDt, dt);
    }


    {
        getPipelineScene()->lights.clear();
        mRegistry.view<TransformC, LightC>().each([&](auto const, TransformC &transform, LightC &light) {
            light.light->setPosition(transform.transform.position);
            getPipelineScene()->lights.push_back(light.light);
        });
    }

    {
        GlfwApp::render(dt);
    }
}

void FlockApp::onResize(int w, int h)
{
    getCamera()->setViewportSize(w, h);
}

void FlockApp::onGui()
{
    if (mPlayerControlActive)
    {
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(15, 15));

        ImGui::SetNextWindowPos(ImVec2(100, 100), ImGuiCond_Always);
        ImGui::SetNextWindowSize(ImVec2(320, 85), ImGuiCond_Always);

        ImGui::Begin("Flock", nullptr,
                     ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize
                         | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav);

        ImGui::Text("%.f points  x%.1f", static_cast<double>(mPlayerController->mScore),
                    static_cast<double>(mPlayerController->mMultiplier));

        ImGui::Separator();

        if (mPlayerController->mNextUpgradeAvailable)
        {
            ImGui::TextColored(ImVec4(.7f, .7f, 1, 1), "Weapon level %d | upgrade in %d",
                               mPlayerController->mWeaponUpgradeLevel, mPlayerController->mBloodLeftToUpgrade);
        }
        else
        {
            ImGui::TextColored(ImVec4(1, .7f, .7f, 1), "Weapon level %d | ultimate form", mPlayerController->mWeaponUpgradeLevel);
        }

        ImGui::Separator();

        ImGui::TextColored(ImVec4(.7f, .7f, .7f, 1), "Best: %.f", static_cast<double>(mHighscore));

        ImGui::End();

        if (mPlayerDead)
        {
            ImGui::SetNextWindowPos(ImVec2(100, 215), ImGuiCond_Always);

            ImGui::Begin("Game Over##Window", nullptr,
                         ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoSavedSettings
                             | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav | ImGuiWindowFlags_AlwaysAutoResize);

            ImGui::TextColored(ImVec4(1, .5f, .2f, 1), "Game Over!");
            ImGui::TextColored(ImVec4(1, .5f, .2f, 1), "Press R to restart");

            ImGui::End();
        }

        ImGui::PopStyleVar(ImGuiStyleVar_WindowPadding);
    }
    else
    {
        ImGui::SetNextWindowPos(ImVec2(400, 20), ImGuiCond_FirstUseEver);
        ImGui::SetNextWindowSize(ImVec2(450, 400), ImGuiCond_FirstUseEver);

        {
            ImGui::Begin("Flock Config");

            if (ImGui::TreeNode("Swarm"))
            {
                ImGui::SliderFloat("Perception Radius", &mSwarm->mPerceptionRadius, 0.5f, 40.f);
                ImGui::SliderFloat("Separation Radius", &mSwarm->mSeparationRadius, 0.1f, 20.f);

                ImGui::Spacing();

                ImGui::SliderFloat("Separation Weight", &mSwarm->mSeparationWeight, 0.f, 10.f);
                ImGui::SliderFloat("Alignment Weight", &mSwarm->mAlignmentWeight, 0.f, 10.f);
                ImGui::SliderFloat("Cohesion Weight", &mSwarm->mCohesionWeight, 0.f, 10.f);
                ImGui::SliderFloat("Steering Weight", &mSwarm->mSteeringWeight, 0.f, 10.f);

                ImGui::Spacing();

                ImGui::SliderFloat("Blindspot Angle", &mSwarm->mBlindspotAngleDeg, 0.f, 360.f);

                ImGui::Spacing();

                ImGui::SliderFloat("Max Acceleration", &mSwarm->mMaxAcceleration, 0.5f, 50.f);
                ImGui::SliderFloat("Max Velocity", &mSwarm->mMaxVelocity, 0.5f, 50.f);

                ImGui::TreePop();
            }

            if (ImGui::TreeNode("Particles"))
            {
                ImGui::SliderFloat("Billboard Size", &mParticleSystem->mConfig.billboardSize, 0.f, 1.f);
                ImGui::SliderFloat("Particle Lifetime", &mParticleSystem->mConfig.particleLifetime, 0.f, 5.f);
                ImGui::SliderInt("Emit per 5ms", &mParticleSystem->mConfig.emitCount, 0, 40);
                ImGui::SliderFloat("Velocity Scale", &mParticleSystem->mConfig.velocityScale, 0.f, 2.f);

                ImGui::TreePop();
            }

            if (ImGui::TreeNode("Rendering"))
            {
                ImGui::SliderFloat("Ambient Light Itns.", &mAmbientLightIntensity, 0.f, 10.f);

                ImGui::Checkbox("Light Heatmap", &mShowDebugClusterHeatmap);

                ImGui::TreePop();
            }

            ImGui::End();
        }
    }
}

bool FlockApp::onMousePosition(double x, double y)
{
    if (mPlayerControlActive)
    {
        auto const dx = static_cast<float>(x - mMouseLastX);
        auto const dy = static_cast<float>(y - mMouseLastY);

        static bool first = true;
        if (first)
        {
            first = false;
            // The first dx / dy is worthless as it is the initial mouse update
        }
        else
        {
            auto const ax = dx / getWindowWidth();
            auto const ay = dy / getWindowWidth();
            mPlayerController->applyMouse(ax, ay);
        }
    }

    mMouseLastX = x;
    mMouseLastY = y;

    return false;
}

bool FlockApp::onKey(int key, int scancode, int action, int mods)
{
    if (glow::glfw::GlfwApp::onKey(key, scancode, action, mods))
        return true;

    // Escape - Quit
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
        requestClose();
        return true;
    }

    // Tab - Toggle player control
    if (key == GLFW_KEY_TAB && action == GLFW_PRESS)
    {
        mPlayerControlActive = !mPlayerControlActive;
        setUsePipelineConfigGui(!mPlayerControlActive);

        if (mPlayerControlActive)
        {
            setCursorMode(glfw::CursorMode::Disabled);
        }
        else
        {
            setCursorMode(glfw::CursorMode::Normal);
        }

        return true;
    }

    // F11 - Toggle fullscreen
    if (key == GLFW_KEY_F11 && action == GLFW_PRESS)
    {
        toggleFullscreen();
        return true;
    }

    // R - Restart on death
    if (key == GLFW_KEY_R && action == GLFW_PRESS)
    {
        if (!mPlayerController->isPlayerAlive())
        {
            teardownGameLogic();
            startupGameLogic();

            mPlayerDead = false;
            mPaused = false;
        }
        return true;
    }

    if (key == GLFW_KEY_P && action == GLFW_PRESS)
    {
        mPaused = !mPaused;
        return true;
    }

    return false;
}

void FlockApp::onPerformShadowPassInstanced(const RenderScene &, const RenderStage &, const CameraData &, UsedProgram &)
{
    mInstancingManager->draw();
}

void FlockApp::onPerformShadowPass(const RenderScene &, const RenderStage &, const CameraData &d, UsedProgram &shader)
{
    drawOpaqueGeometry(shader, d, true);
}

void FlockApp::onRenderDepthPrePass(const RenderScene &, const RenderStage &s, const CameraData &d)
{
    {
        auto shader = mShaderDepthPre->use();
        s.prepareShader(*getPipeline(), mShaderDepthPre, shader);
        shader.setUniform("uView", d.view);
        shader.setUniform("uProj", d.proj);

        drawOpaqueGeometry(shader, d, true);
    }

    {
        auto shader = mShaderDepthPreInstanced->use();
        s.prepareShader(*getPipeline(), mShaderDepthPreInstanced, shader);
        shader.setUniform("uView", d.view);
        shader.setUniform("uProj", d.proj);

        mInstancingManager->draw();
    }
}

void FlockApp::onRenderOpaquePass(const RenderScene &p, const RenderStage &s, const CameraData &d)
{
    {
        auto shader = mShaderOpaqueForward->use();
        s.prepareShader(*getPipeline(), mShaderOpaqueForward, shader);
        shader.setUniform("uView", d.view);
        shader.setUniform("uProj", d.proj);
        shader.setUniform("uCamPos", d.camPos);
        material::IBL::prepareShaderGGX(shader, mEnvironmentMap);

        shader.setUniform("uSunDirection", p.sunDirection);
        shader.setUniform("uSunColor", p.sunColor * p.sunIntensity);
        shader.setUniform("uAmbientIntensity", mAmbientLightIntensity);

        shader.setUniform("uShowClusterHeatmap", mShowDebugClusterHeatmap);
        drawOpaqueGeometry(shader, d, false);
    }

    {
        auto shader = mShaderOpaqueForwardInstanced->use();
        s.prepareShader(*getPipeline(), mShaderOpaqueForwardInstanced, shader);
        shader.setUniform("uView", d.view);
        shader.setUniform("uProj", d.proj);
        shader.setUniform("uCamPos", d.camPos);
        material::IBL::prepareShaderGGX(shader, mEnvironmentMap);

        shader.setUniform("uSunDirection", p.sunDirection);
        shader.setUniform("uSunColor", p.sunColor * p.sunIntensity);
        shader.setUniform("uAmbientIntensity", mAmbientLightIntensity);

        shader.setUniform("uCleanVp", d.cleanVp);
        shader.setUniform("uPrevCleanVp", d.prevCleanVp);

        shader.setUniform("uShowClusterHeatmap", mShowDebugClusterHeatmap);

        mInstancingManager->draw(shader);
    }
}

void FlockApp::onRenderTransparentPass(const RenderScene &, const RenderStage &stage, const CameraData &d)
{
    {
        auto shader = mShaderTransparentInstanced->use();
        stage.prepareShader(*getPipeline(), mShaderTransparentInstanced, shader);
        shader.setUniform("uView", d.view);
        shader.setUniform("uProj", d.proj);
        shader.setUniform("uCamPos", d.camPos);

        shader.setUniform("uCleanVp", d.cleanVp);
        shader.setUniform("uPrevCleanVp", d.prevCleanVp);

        mInstancingManager->drawTransparent(shader);
    }

    {
        mParticleSystem->render(*getPipeline(), stage, d);
    }

    {
        mPrimitiveParticleRenderer->render(*getPipeline(), stage, d);
    }
}

FlockApp::~FlockApp()
{
    FileSaver::storeSavegame(SaveFile{mHighscore});
    //    aion::tracing::write_speedscope_json();
}
}
