#pragma once

#include <cstdint>
#include <memory>
#include <vector>

#include <entt/entity/registry.hpp>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

#include "camera/FollowCameraController.hh"
#include "instancing/InstancingManager.hh"
#include "logic/gameplay/AnimationController.hh"
#include "logic/gameplay/GameController.hh"
#include "logic/gameplay/GameState.hh"
#include "logic/gameplay/Hive.hh"
#include "logic/gameplay/Chalice.hh"
#include "logic/gameplay/GameEvents.hh"
#include "logic/gameplay/PlayerController.hh"
#include "logic/physics/PhysicsEngine.hh"
#include "particles/ParticleSystem.hh"
#include "particles/PrimitiveParticleRenderer.hh"
#include "swarm/Swarm.hh"

namespace flock
{
class FlockApp : public glow::glfw::GlfwApp
{
private:
    static constexpr auto ticksPerSecond = 60.f;

private:
    // == Shaders ==
    glow::SharedProgram mShaderDepthPre;
    glow::SharedProgram mShaderDepthPreInstanced;
    glow::SharedProgram mShaderOpaqueForward;
    glow::SharedProgram mShaderOpaqueForwardInstanced;
    glow::SharedProgram mShaderTransparent;
    glow::SharedProgram mShaderTransparentInstanced;

    // == Meshes ==
    glow::SharedVertexArray mMeshMonkey;
    glow::SharedVertexArray mMeshPlane;
    glow::SharedVertexArray mMeshIcosphere;
    glow::SharedVertexArray mMeshBlood;
    glow::SharedVertexArray mMeshTerrorSquid;

    // == IBL maps ==
    glow::SharedTextureCubeMap mSkyboxMap;
    glow::SharedTextureCubeMap mEnvironmentMap;

    // == Engine Components ==
    std::shared_ptr<InstancingManager> mInstancingManager;
    std::unique_ptr<ParticleSystem> mParticleSystem;
    std::unique_ptr<PrimitiveParticleRenderer> mPrimitiveParticleRenderer;
    std::unique_ptr<GameEvents> mGameEvents;

    // == Logic ==
    entt::registry<uint32_t> mRegistry;
    PhysicsEngine mPhysicsEngine{1.f / ticksPerSecond};
    std::unique_ptr<FollowCameraController> mCameraController;
    std::unique_ptr<Hive> mHive;
    std::unique_ptr<Chalice> mChalice;
    std::unique_ptr<Swarm> mSwarm;
    std::unique_ptr<PlayerController> mPlayerController;
    std::unique_ptr<AnimationController> mAnimationController;
    std::unique_ptr<GameState> mGamePhase;
    std::unique_ptr<GameController> mGameController;

    // == State ==
    bool mPlayerControlActive = true;
    bool mPlayerDead = false;
    double mMouseLastX;
    double mMouseLastY;
    bool mShowDebugClusterHeatmap = false;
    bool mPaused = false;
    float mHighscore = 0.f;

    // == Additional render scene ==
    float mAmbientLightIntensity = 1.f;

private:
    void startupGameCore();
    void startupGameLogic();
    void teardownGameLogic();
    void loadLevel();

    void drawOpaqueGeometry(glow::UsedProgram& shader, glow::pipeline::CameraData const& camData, bool zPreOnly = false);

    void internalUpdate(float dt, float realDt);

protected:
    void init() override;
    void update(float elapsedSeconds) override;
    void render(float elapsedSeconds) override;
    void onResize(int w, int h) override;
    void onGui() override;
    bool onMousePosition(double x, double y) override;
    bool onKey(int key, int scancode, int action, int mods) override;

protected:
    // == Render callback overrides ==
    void onPerformShadowPassInstanced(glow::pipeline::RenderScene const&,
                                      glow::pipeline::RenderStage const&,
                                      glow::pipeline::CameraData const&,
                                      glow::UsedProgram&) override;
    void onPerformShadowPass(glow::pipeline::RenderScene const&,
                             glow::pipeline::RenderStage const&,
                             glow::pipeline::CameraData const&,
                             glow::UsedProgram&) override;
    void onRenderDepthPrePass(glow::pipeline::RenderScene const& p,
                              glow::pipeline::RenderStage const& s,
                              glow::pipeline::CameraData const& d) override;
    void onRenderOpaquePass(glow::pipeline::RenderScene const& p,
                            glow::pipeline::RenderStage const& s,
                            glow::pipeline::CameraData const& d) override;
    void onRenderTransparentPass(glow::pipeline::RenderScene const& p,
                                 glow::pipeline::RenderStage const& s,
                                 glow::pipeline::CameraData const& d) override;

public:
    ~FlockApp() override;
};
}
