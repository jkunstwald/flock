#pragma once

#include <algorithm>
#include <random>

#include <glm/ext.hpp>

#include <glow-extras/camera/Camera.hh>

#include "../util/Misc.hh"

namespace flock
{
class FollowCameraController
{
private:
    glow::camera::SharedCamera const mCamera;

    glm::vec3 mPhysicalPosition{0};
    glm::quat mPhysicalRotation{};

    glm::vec3 mTargetPosition{0};
    glm::quat mTargetRotation{};

    float mCameraInterpolationSpeed = 12.5f;
    float mCameraSensitivity = 20.f;

    float const mCameraShakeInterval = .01f;
    float const mCameraShakeDistance = .05f;

    float mCameraShakeTimer = 0.f;
    float mCameraShakeIntensity = 0.f;

public:
    FollowCameraController(glow::camera::SharedCamera const& cam) : mCamera(cam)
    {
        mPhysicalPosition = mCamera->getPosition();
        mPhysicalRotation = mCamera->handle.getTransform().rotation;
    }

    void update(float dt)
    {
        static std::mt19937 rng;

        mPhysicalPosition = glm::lerp(mPhysicalPosition, mTargetPosition, std::min(dt * mCameraInterpolationSpeed, 1.f));
        mPhysicalRotation = glm::slerp(mPhysicalRotation, mTargetRotation, std::min(dt * mCameraSensitivity, 1.f));

        mCameraShakeTimer += dt;
        if (mCameraShakeTimer >= mCameraShakeInterval)
        {
            mCameraShakeTimer = 0.f;
            mPhysicalPosition
                += mPhysicalRotation
                   * glm::vec3(util::getRandomDirection2D(rng) * (mCameraShakeDistance * mCameraShakeIntensity), 0);
        }

        mCamera->handle.physicalState().transform.position = mPhysicalPosition;
        mCamera->handle.physicalState().transform.rotation = mPhysicalRotation;
    }

    void snapToTarget()
    {
        mPhysicalPosition = mTargetPosition;
        mPhysicalRotation = mTargetRotation;

        mCamera->handle.physicalState().transform.position = mPhysicalPosition;
        mCamera->handle.physicalState().transform.rotation = mPhysicalRotation;
    }

    void setTargetPosition(glm::vec3 const& pos) { mTargetPosition = pos; }
    void setTargetRotation(glm::quat const& rot) { mTargetRotation = rot; }
    void setCamShakeIntensity(float intensity) { mCameraShakeIntensity = intensity; }
};
}
