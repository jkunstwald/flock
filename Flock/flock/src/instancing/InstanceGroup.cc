#include "InstanceGroup.hh"

flock::InstanceGroup::InstanceGroup(const glow::SharedVertexArray &mesh, const flock::MaterialC &mat)
  : mMesh(mesh), mMaterialZero(mat)
{
    mArrayBuffer = glow::ArrayBuffer::create(InstanceVertexData::Attributes());
    mArrayBuffer->setDivisor(1);
    mMesh->bind().attach(mArrayBuffer);
}

unsigned flock::InstanceGroup::getNewInstanceId()
{
    return mRunningInstanceId++;
}

void flock::InstanceGroup::addInstanceData(const glm::mat4 &model, const glm::mat4 &prevModel)
{
    mInstanceData.emplace_back(InstanceVertexData{model[0], model[1], model[2], model[3], prevModel[0], prevModel[1],
                                                  prevModel[2], prevModel[3], 999});
}

void flock::InstanceGroup::uploadArrayBufferData()
{
    mArrayBuffer->bind().setData(mInstanceData);
    mInstanceData.clear();
}

void flock::InstanceGroup::bindMaterialsToShader(glow::UsedProgram &shader)
{
    shader.setUniform("uRoughness", mMaterialZero.roughness);
    shader.setUniform("uMetallic", mMaterialZero.metallic);
    shader.setUniform("uAlbedo", mMaterialZero.albedo);
}

flock::SharedInstanceGroup flock::InstanceGroup::create(const glow::SharedVertexArray &mesh, const flock::MaterialC &mat)
{
    return std::make_shared<InstanceGroup>(mesh, mat);
}
