#pragma once

#include <memory>
#include <vector>

#include <glm/glm.hpp>

#include <glow/common/shared.hh>
#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/ElementArrayBuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/VertexArray.hh>
#include <glow/objects/VertexArrayAttribute.hh>

#include "../logic/MaterialC.hh"
#include "InstanceVertexData.hh"

namespace flock
{
GLOW_SHARED(class, InstanceGroup);
class InstanceGroup
{
private:
    glow::SharedVertexArray mMesh;
    flock::MaterialC mMaterialZero;

    glow::SharedArrayBuffer mArrayBuffer;
    std::vector<InstanceVertexData> mInstanceData;
    unsigned mRunningInstanceId = 0;

public:
    InstanceGroup(const glow::SharedVertexArray& mesh, const flock::MaterialC& mat);

    unsigned getNewInstanceId();

    void addInstanceData(glm::mat4 const& model, glm::mat4 const& prevModel);

    void uploadArrayBufferData();

    void bindMaterialsToShader(glow::UsedProgram& shader);

    inline void draw() { mMesh->bind().draw(); }

    static SharedInstanceGroup create(const glow::SharedVertexArray& mesh, const flock::MaterialC& mat);
};
}
