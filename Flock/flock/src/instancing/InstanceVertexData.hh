#pragma once

#include <glm/glm.hpp>
#include <glow/objects/ArrayBufferAttribute.hh>

#include <vector>

namespace flock
{
struct InstanceVertexData
{
    // glm::mat4 model;
    // glm::mat4 prevModel;

    // Vertex Arrays do not support matrices trivially
    // This is a very ugly solution, look into UBOs or persistent mapped buffers
    // see https://ferransole.wordpress.com/2014/06/08/persistent-mapped-buffers/

    // These are column-major
    // Pass using mat[0], mat[1], mat[2], mat[3]
    // Reconstruct in shader using mat4(C0, C1, C2, C3);

    glm::vec4 modelC0;
    glm::vec4 modelC1;
    glm::vec4 modelC2;
    glm::vec4 modelC3;

    glm::vec4 prevModelC0;
    glm::vec4 prevModelC1;
    glm::vec4 prevModelC2;
    glm::vec4 prevModelC3;

    uint32_t materialId = 0; // unused

    static std::vector<glow::ArrayBufferAttribute> Attributes()
    {
        return {
            {&InstanceVertexData::modelC0, "aModelC0"},         {&InstanceVertexData::modelC1, "aModelC1"},
            {&InstanceVertexData::modelC2, "aModelC2"},         {&InstanceVertexData::modelC3, "aModelC3"},
            {&InstanceVertexData::prevModelC0, "aPrevModelC0"}, {&InstanceVertexData::prevModelC1, "aPrevModelC1"},
            {&InstanceVertexData::prevModelC2, "aPrevModelC2"}, {&InstanceVertexData::prevModelC3, "aPrevModelC3"},
            {&InstanceVertexData::materialId, "aMaterialId"},
        };
    }
};
}
