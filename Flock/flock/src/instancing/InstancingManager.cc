#include "InstancingManager.hh"

flock::InstancedMeshC flock::InstancingManager::createInstance(const glow::SharedVertexArray &mesh,
                                                               const flock::MaterialC &material,
                                                               bool immovable)
{
    unsigned const meshId = mesh->getObjectName();
    unsigned assignedInstanceId;

    auto classIt = mInstanceClasses.find(meshId);
    if (classIt != mInstanceClasses.end())
    {
        assignedInstanceId = classIt->second->getNewInstanceId();
    }
    else
    {
        mInstanceClasses.emplace(std::pair<unsigned, SharedInstanceGroup>(meshId, InstanceGroup::create(mesh, material)));
        assignedInstanceId = mInstanceClasses[meshId]->getNewInstanceId();
    }

    return InstancedMeshC{meshId, assignedInstanceId, immovable};
}

flock::InstancedTransparentMeshC flock::InstancingManager::createTransparentInstance(const glow::SharedVertexArray &mesh,
                                                                                     const flock::MaterialC &material,
                                                                                     bool immovable)
{
    unsigned const meshId = mesh->getObjectName();
    unsigned assignedInstanceId;

    auto classIt = mTransparentInstanceClasses.find(meshId);
    if (classIt != mTransparentInstanceClasses.end())
    {
        assignedInstanceId = classIt->second->getNewInstanceId();
    }
    else
    {
        mTransparentInstanceClasses.emplace(std::pair<unsigned, SharedInstanceGroup>(meshId, InstanceGroup::create(mesh, material)));
        assignedInstanceId = mTransparentInstanceClasses[meshId]->getNewInstanceId();
    }

    return InstancedTransparentMeshC{meshId, assignedInstanceId, immovable};
}

void flock::InstancingManager::update(entt::registry<uint32_t> &registry)
{
    // Rebuild Instance Data
    {
        registry.view<TransformC, InstancedMeshC>().each([&](const auto, TransformC &transform, InstancedMeshC &instance) {
            auto const model = transform.transform.getModelMatrix();
            mInstanceClasses[instance.classId]->addInstanceData(model, transform.previousModelMatrix);
            transform.previousModelMatrix = model;
        });

        registry.view<TransformC, InstancedTransparentMeshC>().each(
            [&](const auto, TransformC &transform, InstancedTransparentMeshC &instance) {
                auto const model = transform.transform.getModelMatrix();
                mTransparentInstanceClasses[instance.classId]->addInstanceData(model, transform.previousModelMatrix);
                transform.previousModelMatrix = model;
            });
    }


    // Upload to GPU
    {
        for (auto &instanceClass : mInstanceClasses)
        {
            instanceClass.second->uploadArrayBufferData();
        }

        for (auto &instanceClass : mTransparentInstanceClasses)
        {
            instanceClass.second->uploadArrayBufferData();
        }
    }
}

void flock::InstancingManager::draw(glow::UsedProgram &shader)
{
    for (auto &instanceClass : mInstanceClasses)
    {
        instanceClass.second->bindMaterialsToShader(shader);
        instanceClass.second->draw();
    }
}

void flock::InstancingManager::drawTransparent(glow::UsedProgram &shader)
{
    shader.setUniform("uMaterialAlpha", .5f);

    for (auto &instanceClass : mTransparentInstanceClasses)
    {
        instanceClass.second->bindMaterialsToShader(shader);
        instanceClass.second->draw();
    }
}

void flock::InstancingManager::draw()
{
    for (auto &instanceClass : mInstanceClasses)
    {
        instanceClass.second->draw();
    }
}
