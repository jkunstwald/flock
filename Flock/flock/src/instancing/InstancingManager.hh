#pragma once

#include <tuple>
#include <unordered_map>
#include <vector>

#include <glm/glm.hpp>

#include <aion/Tracer.hh>

#include <entt/entity/registry.hpp>

#include "../logic/TransformC.hh"
#include "InstanceGroup.hh"
#include "InstanceVertexData.hh"

namespace flock
{
struct InstancedMeshC
{
    unsigned classId;       ///< ID of this instances class (hash of Mesh)
    unsigned instanceId;    ///< ID of this instance, unique within its class
    bool immovable = false; ///< If true, the instance can never move
};

struct InstancedTransparentMeshC
{
    unsigned classId;       ///< ID of this instances class (hash of Mesh)
    unsigned instanceId;    ///< ID of this instance, unique within its class
    bool immovable = false; ///< If true, the instance can never move
};

class InstancingManager
{
private:
    std::unordered_map<unsigned, SharedInstanceGroup> mInstanceClasses{};
    std::unordered_map<unsigned, SharedInstanceGroup> mTransparentInstanceClasses{};

public:
    InstancedMeshC createInstance(glow::SharedVertexArray const &mesh, MaterialC const &material = MaterialC{}, bool immovable = false);
    InstancedTransparentMeshC createTransparentInstance(glow::SharedVertexArray const &mesh,
                                                        MaterialC const &material = MaterialC{},
                                                        bool immovable = false);

    void update(entt::registry<uint32_t> &registry);

    void draw(glow::UsedProgram &shader);
    void drawTransparent(glow::UsedProgram &shader);
    void draw();
};
}
