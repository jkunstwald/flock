#pragma once

#include <glow-extras/pipeline/lights/Light.hh>

namespace flock
{
struct LightC
{
    glow::pipeline::SharedLight light;
};
}
