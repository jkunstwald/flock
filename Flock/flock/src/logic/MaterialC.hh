#pragma once

#include <glm/vec3.hpp>

namespace flock
{
struct MaterialC
{
    glm::vec3 albedo = glm::vec3(1);
    float roughness = .5f;
    float metallic = .75f;
};
}
