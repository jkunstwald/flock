#pragma once

#include <algorithm>
#include <cassert>

namespace flock
{
struct OneshotTimerC
{
private:
    float mTimer = 0.f;
    float /*const*/ mDuration = .5f;

public:
    OneshotTimerC(float duration = .5f) : mDuration(duration) { assert(duration > 0.f); }

    /// Returns true if the oneshot is over
    bool tick(float dt)
    {
        mTimer += dt;
        if (mTimer >= mDuration)
            return true;
        return false;
    }

    float getPercentage() { return std::min(1.f, mTimer / mDuration); }
};
}
