#pragma once

#include <glm/ext.hpp>
#include <glm/glm.hpp>

#include "../util/Misc.hh"

namespace flock
{
struct RandomTravelAnimC
{
private:
    static constexpr auto directionChangeInterval = 5.f; ///< Time in seconds before a new random direction is chosen

private:
    float mVelocity; ///< Travel velocity in m/s
    float mTime = directionChangeInterval;
    glm::vec3 mCurrentDir = glm::vec3(0, 1, 0);
    glm::vec3 mNextDir = glm::vec3(0, 1, 0);

public:
    RandomTravelAnimC(float vel = 1.f) : mVelocity(vel) {}

    /// Returns the travel position offset for this tick
    glm::vec3 tick(float dt, std::mt19937& rng)
    {
        mTime += dt;
        if (mTime >= directionChangeInterval)
        {
            mNextDir = util::getRandomDirection(rng);
            mTime = 0.f;
        }

        mCurrentDir = glm::lerp(mCurrentDir, mNextDir, dt);
        return mCurrentDir * mVelocity;
    }

    [[nodiscard]] constexpr float getVelocity() const noexcept { return mVelocity; }
};
}
