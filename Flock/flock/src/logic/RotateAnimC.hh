#pragma once

#include <glm/ext.hpp>
#include <glm/glm.hpp>

namespace flock
{
struct RotateAnimC
{
private:
    float mVelocity; ///< Travel velocity in deg/s

public:
    RotateAnimC(float vel = 1.f) : mVelocity(vel) {}

    /// Returns the rotation offset for this tick
    glm::vec3 tick(float dt) { return {0, glm::radians(dt * mVelocity), 0}; }
};
}
