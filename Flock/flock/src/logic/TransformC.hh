#pragma once

#include <glow/math/transform.hh>

namespace flock
{
struct TransformC
{
    glow::transform transform{};
    glm::mat4 previousModelMatrix{};
    bool drawingActive = true;
};
}
