#pragma once

#include <algorithm>
#include <random>

#include <entt/entity/registry.hpp>

#include "../../util/Misc.hh"
#include "../OneshotTimerC.hh"
#include "../RandomTravelAnimC.hh"
#include "../RotateAnimC.hh"
#include "../TransformC.hh"

namespace flock
{
class AnimationController
{
private:
    std::mt19937 mRng{};
    entt::registry<uint32_t>* const mRegistry;

    glm::vec3 const mPlayingFieldCenter = glm::vec3{0};
    float const mRandomTravelMaxDistanceSq = std::pow(250.f, 2.f);

public:
    AnimationController(entt::registry<uint32_t>* reg) : mRegistry(reg) {}

    void tick(float dt)
    {
        mRegistry->view<OneshotTimerC>().each([&](auto const ent, OneshotTimerC& timer) {
            if (timer.tick(dt))
                mRegistry->destroy(ent);
        });

        mRegistry->view<TransformC, RandomTravelAnimC>().each([&](auto const, TransformC& transform, RandomTravelAnimC& anim) {
            auto const distanceToCenterSq = glm::distance2(transform.transform.position, mPlayingFieldCenter);
            auto const& pos = transform.transform.position;

            // More movement towards the center the farther away this entity is
            auto const travelDir
                = glm::lerp(anim.tick(dt, mRng), util::safeNormalize(mPlayingFieldCenter - pos) * anim.getVelocity(),
                            util::smoothstep(0.f, mRandomTravelMaxDistanceSq, distanceToCenterSq));

            transform.transform.position = pos + travelDir * dt;
        });

        mRegistry->view<TransformC, RotateAnimC>().each(
            [&](auto const, TransformC& transform, RotateAnimC& anim) { transform.transform.rotate(anim.tick(dt)); });
    }
};

}
