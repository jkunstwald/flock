#include "Chalice.hh"

#include "../../util/Misc.hh"
#include "../RotateAnimC.hh"
#include "../TransformC.hh"
#include "ChaliceBloodC.hh"
#include "PlayerController.hh"

flock::Chalice::Chalice(entt::registry<uint32_t> *reg,
                        flock::PlayerController *playerController,
                        GameEvents *events,
                        const glow::SharedVertexArray &bloodMesh)
  : mRegistry(reg), mPlayerController(playerController), mEvents(events), mBloodMesh(bloodMesh)
{
}

void flock::Chalice::spawnBlood(const glm::vec3 &pos, flock::InstancingManager *instMan)
{
    if (mBloodAmount >= maxBlood)
        return;

    auto const ent = mRegistry->create();
    mRegistry->assign<TransformC>(ent, glow::transform(pos, {}, glm::vec3(.5f)));
    mRegistry->assign<ChaliceBloodC>(ent);
    mRegistry->assign<InstancedTransparentMeshC>(ent, instMan->createTransparentInstance(mBloodMesh, mBloodMaterial));
    mRegistry->assign<RotateAnimC>(ent, RotateAnimC(180.f));

    ++mBloodAmount;
}

void flock::Chalice::update(float dt)
{
    auto const &playerPos = mPlayerController->getPosition();
    auto const playerFiring = mPlayerController->isFiring();

    mRegistry->view<TransformC, ChaliceBloodC>().each([&](auto const ent, TransformC &transform, ChaliceBloodC &blood) {
        auto const distanceVec = playerPos - transform.transform.position;
        auto const distanceSq = glm::length2(distanceVec);

        if (distanceSq <= bloodRadiusSq)
        {
            // Collected
            mEvents->trigger<GameEvents::BloodPickup>();
            mRegistry->destroy(ent);
            --mBloodAmount;
        }
        else if (distanceSq >= bloodDiscardRadiusSq)
        {
            // Discarded
            mRegistry->destroy(ent);
            --mBloodAmount;
        }
        else
        {
            // Integrate

            blood.velocity = util::exponentialDecayLerp(blood.velocity, glm::vec3(0), bloodVelDampingLambda, dt);

            if (!playerFiring)
            {
                // Attraction to player
                blood.velocity += util::safeNormalize(distanceVec, distanceSq) * (bloodAttractionAccell * dt);
            }
            else
            {
                // Travel away from center
                blood.velocity += util::safeNormalize(transform.transform.position) * (bloodTravelAccell * dt);
            }

            transform.transform.position += blood.velocity * dt;
        }
    });
}
