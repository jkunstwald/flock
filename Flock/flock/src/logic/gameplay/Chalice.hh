#pragma once

#include <entt/entity/registry.hpp>

#include <glow-extras/colors/color.hh>

#include "../../instancing/InstancingManager.hh"
#include "GameEvents.hh"

/**
 * The Chalice
 *
 * Manages "Blood", the small upgrade diamonds
 */

namespace flock
{
class PlayerController;

class Chalice
{
private:
    static constexpr auto bloodVelDampingLambda = 5.f;

    static constexpr auto bloodTravelAccell = 15.f;
    static constexpr auto bloodAttractionAccell = 700.f;

    static constexpr auto bloodRadius = 2.5f; ///< Radius in which blood is collected by the player, in meters
    static constexpr auto bloodRadiusSq = bloodRadius * bloodRadius;

    static constexpr auto bloodDiscardRadius = 300.f; ///< Distance from player after which blood is destroyed, in meters
    static constexpr auto bloodDiscardRadiusSq = bloodDiscardRadius * bloodDiscardRadius;

    static constexpr auto maxBlood = 200;

    entt::registry<uint32_t>* const mRegistry;
    PlayerController* const mPlayerController;
    GameEvents* const mEvents;

    glow::SharedVertexArray const mBloodMesh;
    MaterialC const mBloodMaterial = MaterialC{glow::colors::color::from_hex("22FFAA").to_rgb() * 8.f, 1.f, 1.f};

    unsigned mBloodAmount = 0;

public:
    Chalice(entt::registry<uint32_t>* reg, PlayerController* playerController, GameEvents* events, glow::SharedVertexArray const& bloodMesh);

    void spawnBlood(glm::vec3 const& pos, InstancingManager* instMan);

    void update(float dt);
};

}
