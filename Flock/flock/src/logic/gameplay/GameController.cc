#include "GameController.hh"

#define RAND(_min_, _max_) std::uniform_int_distribution<unsigned>(_min_, _max_)(mRng)
void flock::GameController::startNewChapter()
{
    mCurrentChapter.length = RAND(5, 6 + mGlobalDifficultyModifier);
    mCurrentChapter.squidAmountRange = 1 + mGlobalDifficultyModifier;
    mCurrentChapter.endsWithBoss = (RAND(0, 10) >= 4u);

    mCurrentChapter.bossPrologue = RAND(1, 2);
    mCurrentChapter.bossAmount = RAND(1, 1 + mGlobalDifficultyModifier);

    mCurrentChapter.chapterEpilogue = 1;

    switch (mGlobalDifficultyModifier)
    {
    case 0:
        mGamePhase->enterState(GameState::SwarmMode::Slow, GameState::AtmosphereMode::BrightDay, 1.f);
        break;
    case 1:
        mGamePhase->enterState(GameState::SwarmMode::Normal, GameState::AtmosphereMode::DimDay, 1.f);
        break;
    case 2:
    case 3:
    default:
        mGamePhase->enterState(GameState::SwarmMode::Fast, GameState::AtmosphereMode::Night, 1.f);
        break;
    }

    mState = State{};
    mState.intervalsLeft = mCurrentChapter.length;
    mState.bossPending = mCurrentChapter.endsWithBoss;
    mState.bossPrologueLeft = mCurrentChapter.bossPrologue;
    mState.epiloguePauseLeft = mCurrentChapter.chapterEpilogue;
}

void flock::GameController::onChapterComplete()
{
    ++mChapterNumber;

    // Increase difficulty modifier every N chapters
    if (mChapterNumber % 2 == 0)
    {
        if (mGlobalDifficultyModifier < maxDifficulty)
            ++mGlobalDifficultyModifier;
    }

    startNewChapter();
}

void flock::GameController::onBossSpawn()
{
    mEvents->trigger<GameEvents::SpawnTerrorSquids>(mCurrentChapter.bossAmount);
    mGamePhase->enterState(GameState::SwarmMode::Relentless, GameState::AtmosphereMode::Bloodmoon, 4.f);

    mState.bossPending = false;
    mState.bossAlive = true;
}

void flock::GameController::onBossDeath(const flock::GameEvents::BossDeath &)
{
    mGamePhase->enterState(GameState::SwarmMode::Harmless, GameState::AtmosphereMode::Haze, .85f);

    mState.bossAlive = false;
}

void flock::GameController::onPlayerWeaponUpgrade(const flock::GameEvents::WeaponUpgrade &)
{
    mGamePhase->enterPostprocessState(GameState::PostProcessMode::PostUpgrade, 10.f);

    mSlowMoActive = true;
    mSlowMoDuration = 6.f;
    mTimeScale = slowMoTimeScale;
}

void flock::GameController::onSquidSpawn()
{
    auto const spawnAmount = RAND(1, mCurrentChapter.squidAmountRange);
    mEvents->trigger<GameEvents::SpawnSquids>(spawnAmount);

    --mState.intervalsLeft;
}

void flock::GameController::onChapterIntervalProgression()
{
    if (mState.intervalsLeft == 0)
    {
        if (mState.bossPending)
        {
            if (mState.bossPrologueLeft == 0)
            {
                // Spawn boss
                onBossSpawn();
            }
            else
            {
                // Do nothing
                --mState.bossPrologueLeft;
            }
        }
        else
        {
            if (mState.epiloguePauseLeft == 0)
            {
                // Enter new chapter
                onChapterComplete();
            }
            else
            {
                // Do nothing
                --mState.epiloguePauseLeft;
            }
        }
    }
    else
    {
        // Spawn squids
        onSquidSpawn();
    }
}

flock::GameController::GameController(flock::GameState *phase, flock::GameEvents *events)
  : mGamePhase(phase), mEvents(events)
{
    startNewChapter();
    mEvents->dispatcher().sink<GameEvents::BossDeath>().connect<&GameController::onBossDeath>(this);
    mEvents->dispatcher().sink<GameEvents::WeaponUpgrade>().connect<&GameController::onPlayerWeaponUpgrade>(this);
}

void flock::GameController::update(float dt, float undistortedDt)
{
    if (mState.bossAlive)
    {
        // Do nothing, onBossDeath will be called based on an event
    }
    else
    {
        mChapterIntervalTime += dt;
        if (mChapterIntervalTime >= chapterIntervalLength)
        {
            onChapterIntervalProgression();
            mChapterIntervalTime = 0.f;
        }
    }

    if (mSlowMoActive)
    {
        mSlowMoDuration -= undistortedDt;
        if (mSlowMoDuration <= 0.f)
        {
            mSlowMoActive = false;
            mGamePhase->enterPostprocessState(GameState::PostProcessMode::Normal, 1.f);
        }
    }
    else
    {
        if (mTimeScale < 1.f)
            mTimeScale = glm::lerp(mTimeScale, 1.f, undistortedDt * slowMoLerpSpeed);
    }
}
#undef RAND
