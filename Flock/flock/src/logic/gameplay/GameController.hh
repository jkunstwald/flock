#pragma once

#include <iostream>
#include <random>

#include "GameState.hh"
#include "Hive.hh"

namespace flock
{
class GameController
{
private:
    static constexpr auto chapterIntervalLength = 7.5f; ///< Length of a chapter interval in seconds
    static constexpr auto firstChapterDelay = 3.f; ///< Amount of time that is waited until the first spawn at game start
    static constexpr auto maxDifficulty = 3u;

    static constexpr auto slowMoTimeScale = .35f;
    static constexpr auto slowMoLerpSpeed = 1.5f;

    struct Chapter
    {
        unsigned length = 1;           ///< Length of the chapter, in chapter intervals
        unsigned squidAmountRange = 0; ///< Maximum amount of squids spawned during the chapter per step (1 - amount)
        bool endsWithBoss = false;     ///< Whether a boss spawns at the end of the chapter

        unsigned bossPrologue = 0; ///< Length of the pause before the boss spawns, in chapter intervals
        unsigned bossAmount = 0;   ///< Amounts of terror squids spawned at the end of the chapter

        unsigned chapterEpilogue = 0; ///< Length of the pause after all bosses are dead / the chapter is over, in chapter intervals
    };

    struct State
    {
        unsigned intervalsLeft = 0; ///< Amount of chapter intervals left

        unsigned bossPrologueLeft = 0; ///< Amount of pause intervals left before boss spawn
        bool bossPending = false;      ///< If a boss is pending for spawn after the chapter is over
        bool bossAlive = false;        ///< If a boss is currently active

        unsigned epiloguePauseLeft = 0; ///< Amount of pause intervals left after chapter end
    };

private:
    std::mt19937 mRng{};
    GameState* const mGamePhase;
    GameEvents* const mEvents;

    unsigned mGlobalDifficultyModifier = 0;

    unsigned mChapterNumber = 0;
    Chapter mCurrentChapter;
    float mChapterIntervalTime = chapterIntervalLength - firstChapterDelay;
    State mState{};

    float mTimeScale = 1.f;
    bool mSlowMoActive = false;
    float mSlowMoDuration = 0.f;

private:
    void startNewChapter();
    void onChapterComplete();

    void onBossSpawn();
    void onSquidSpawn();
    void onChapterIntervalProgression();

    void onBossDeath(GameEvents::BossDeath const&);
    void onPlayerWeaponUpgrade(GameEvents::WeaponUpgrade const&);

public:
    GameController(GameState* phase, GameEvents* events);

    void update(float dt, float undistortedDt);

    constexpr float getTimeScale() const noexcept { return mTimeScale; }
};
}
