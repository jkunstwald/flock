#pragma once

#include <entt/signal/dispatcher.hpp>

namespace flock
{
class GameEvents
{
public:
    // -- Source: Chalice --
    struct BloodPickup
    {
    };

    // -- Source: PlayerController --
    struct WeaponUpgrade
    {
    };

    // -- Source: GameController --
    struct SpawnSquids
    {
        unsigned amount = 1;
    };

    struct SpawnTerrorSquids
    {
        unsigned amount = 1;
    };

    // -- Source: Hive --
    struct BossDeath
    {
        // Triggered when the last boss dies
    };


private:
    entt::dispatcher mDispatcher{};

public:
    entt::dispatcher& dispatcher() { return mDispatcher; }

    template <typename Event, typename... Args>
    inline void trigger(Args&&... args)
    {
        mDispatcher.enqueue<Event>(std::forward<Args>(args)...);
    }

    //    template <typename Event, typename... Args>
    //    inline void enqueue(Args&&... args)
    //    {
    //        mDispatcher.enqueue<Event>(std::forward<Args>(args)...);
    //    }

    void emitEvents() { mDispatcher.update(); }
};
}
