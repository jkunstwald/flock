#include "GameState.hh"

void flock::GameState::buildTargetState()
{
    switch (mTargetAtmosphereMode)
    {
    case AtmosphereMode::BrightDay:
        mTargetState.atmoScatterIntensity = 1.f;
        mTargetState.atmoScatterDensity = 0.001f;
        mTargetState.atmoScatterColor = {.5f, .6f, .7f};
        break;
    case AtmosphereMode::DimDay:
        mTargetState.atmoScatterIntensity = .5f;
        mTargetState.atmoScatterDensity = 0.001f;
        mTargetState.atmoScatterColor = {.5f, .6f, .7f};
        break;
    case AtmosphereMode::Night:
        mTargetState.atmoScatterIntensity = .25f;
        mTargetState.atmoScatterDensity = 0.001f;
        mTargetState.atmoScatterColor = {.5f, .6f, .7f};
        break;
    case AtmosphereMode::Haze:
        mTargetState.atmoScatterIntensity = 1.f;
        mTargetState.atmoScatterDensity = 0.002f;
        mTargetState.atmoScatterColor = glm::vec3{203 / 255.f, 226 / 255.f, 110 / 255.f} * .75f;
        break;
    case AtmosphereMode::Bloodmoon:
    case AtmosphereMode::TerrorBloodmoon:
        mTargetState.atmoScatterIntensity = .5f;
        mTargetState.atmoScatterDensity = 0.005f;
        mTargetState.atmoScatterColor = {.886f, .4314f, 0.4314f};
        break;
    }

    switch (mTargetSwarmMode)
    {
    case SwarmMode::Harmless:
        mTargetState.swarmChaseType = Swarm::EDistanceType::InverseLinear;
        mTargetState.swarmMaxVelocity = 5.f;
        mTargetState.swarmSteeringWeight = .25f;
        break;
    case SwarmMode::Slow:
        mTargetState.swarmChaseType = Swarm::EDistanceType::Linear;
        mTargetState.swarmMaxVelocity = 25.f;
        mTargetState.swarmSteeringWeight = .25f;
        break;
    case SwarmMode::Normal:
        mTargetState.swarmChaseType = Swarm::EDistanceType::Linear;
        mTargetState.swarmMaxVelocity = 35.f;
        mTargetState.swarmSteeringWeight = .35f;
        break;
    case SwarmMode::Fast:
        mTargetState.swarmChaseType = Swarm::EDistanceType::Linear;
        mTargetState.swarmMaxVelocity = 45.f;
        mTargetState.swarmSteeringWeight = .5f;
        break;
    case SwarmMode::VeryFast:
        mTargetState.swarmChaseType = Swarm::EDistanceType::Linear;
        mTargetState.swarmMaxVelocity = 55.f;
        mTargetState.swarmSteeringWeight = .6f;
        break;
    case SwarmMode::Relentless:
        mTargetState.swarmChaseType = Swarm::EDistanceType::Quadratic;
        mTargetState.swarmMaxVelocity = 55.f;
        mTargetState.swarmSteeringWeight = .25f;
    }

    switch (mTargetPostprocessMode)
    {
    case PostProcessMode::Normal:
        mTargetState.exposure = 1.5f;
        mTargetState.contrast = 1.6f;
        mTargetState.sharpening = .2f;
        break;
    case PostProcessMode::PostUpgrade:
        mTargetState.exposure = 2.5f;
        mTargetState.contrast = 2.f;
        mTargetState.sharpening = .8f;
        break;
    }
}

void flock::GameState::applyCurrentState()
{
    mRenderScene->atmoScatterIntensity = mCurrentState.atmoScatterIntensity;
    mRenderScene->atmoScatterDensity = mCurrentState.atmoScatterDensity;
    mRenderScene->atmoScatterFogColor = mCurrentState.atmoScatterColor;

    mRenderScene->exposure = mCurrentState.exposure;
    mRenderScene->contrast = mCurrentState.contrast;
    mRenderScene->sharpenStrength = mCurrentState.sharpening;

    mSwarm->mSteeringWeight = mCurrentState.swarmSteeringWeight;
    mSwarm->mSteeringTargetType = mCurrentState.swarmChaseType;
    mSwarm->mMaxVelocity = mCurrentState.swarmMaxVelocity;
}

void flock::GameState::lerpToTarget(float dt)
{
#define SMOOTH_VAL(_val_, _a_) mCurrentState._val_ = glm::lerp(mCurrentState._val_, mTargetState._val_, _a_)
#define JUMP_VAL(_val_) mCurrentState._val_ = mTargetState._val_

    auto const alpha = dt * mTargetLerpSpeed;

    SMOOTH_VAL(atmoScatterIntensity, alpha);
    SMOOTH_VAL(atmoScatterColor, alpha);
    SMOOTH_VAL(atmoScatterDensity, alpha);

    SMOOTH_VAL(swarmSteeringWeight, alpha);
    SMOOTH_VAL(swarmMaxVelocity, alpha);
    JUMP_VAL(swarmChaseType);

    auto const postAlpha = dt * mTargetLerpSpeedPost;

    SMOOTH_VAL(contrast, postAlpha);
    SMOOTH_VAL(exposure, postAlpha);
    SMOOTH_VAL(sharpening, postAlpha);

#undef SMOOTH_VAL
#undef JUMP_VAL
}

flock::GameState::GameState(glow::pipeline::RenderScene* scene, flock::Swarm* swarm)
  : mRenderScene(scene), mSwarm(swarm)
{
    //mCurrentState.atmoScatterIntensity = mRenderScene->aoIntensity;
    mCurrentState.atmoScatterColor = mRenderScene->atmoScatterFogColor;
    mCurrentState.atmoScatterDensity = mRenderScene->atmoScatterDensity;

    mCurrentState.exposure = mRenderScene->exposure;
    mCurrentState.contrast = mRenderScene->contrast;
    mCurrentState.sharpening = mRenderScene->sharpenStrength;

    mCurrentState.swarmSteeringWeight = mSwarm->mSteeringWeight;
    mCurrentState.swarmMaxVelocity = mSwarm->mMaxVelocity;
    mCurrentState.swarmChaseType = mSwarm->mSteeringTargetType;

    buildTargetState();
}

void flock::GameState::update(float dt)
{
    mTime += dt;

    lerpToTarget(dt);
    applyCurrentState();
}

void flock::GameState::enterState(flock::GameState::SwarmMode swarmMode, flock::GameState::AtmosphereMode atmoMode, float lerpSpeed)
{
    mTargetSwarmMode = swarmMode;
    mTargetAtmosphereMode = atmoMode;
    mTargetLerpSpeed = lerpSpeed;

    buildTargetState();
}

void flock::GameState::enterPostprocessState(flock::GameState::PostProcessMode postMode, float lerpSpeed)
{
    mTargetPostprocessMode = postMode;
    mTargetLerpSpeedPost = lerpSpeed;

    buildTargetState();
}
