#pragma once

#include <vector>

#include <glm/glm.hpp>

#include <glow-extras/pipeline/RenderScene.hh>

#include "../../swarm/Swarm.hh"

namespace flock
{
class GameState
{
public:
    enum class AtmosphereMode
    {
        BrightDay,
        DimDay,
        Night,
        Haze,           // Yellow, dense fog
        Bloodmoon,      // Red fog
        TerrorBloodmoon // Pulsing
    };

    enum class SwarmMode
    {
        Harmless,
        Slow,
        Normal,
        Fast,
        VeryFast,
        Relentless // Quadratic steering distance
    };

    enum class PostProcessMode
    {
        Normal,
        PostUpgrade // Overexposed
    };

private:
    static constexpr auto stateChangeSpeed = 1.f; ///< Multiplier for delta time when lerping to a new state, more is faster


    struct State
    {
        float atmoScatterDensity;
        float atmoScatterIntensity;
        glm::vec3 atmoScatterColor;

        float swarmSteeringWeight;
        float swarmMaxVelocity;
        Swarm::EDistanceType swarmChaseType;

        float exposure;
        float sharpening;
        float contrast;
    };

    struct PostProcessState
    {
    };

private:
    glow::pipeline::RenderScene* const mRenderScene;
    Swarm* const mSwarm;

    float mTime = 0.f;
    unsigned mPhase = 0;
    bool mLastPhaseReached = false;

    // Swarm and Atmosphere
    State mCurrentState;

    SwarmMode mTargetSwarmMode = SwarmMode::Harmless;
    AtmosphereMode mTargetAtmosphereMode = AtmosphereMode::Haze;
    float mTargetLerpSpeed = stateChangeSpeed;
    State mTargetState;

    // Postprocessing
    PostProcessMode mTargetPostprocessMode = PostProcessMode::Normal;
    float mTargetLerpSpeedPost = stateChangeSpeed;

private:
    void buildTargetState();
    void applyCurrentState();
    void lerpToTarget(float dt);

public:
    GameState(glow::pipeline::RenderScene* scene, Swarm* swarm);

    void update(float dt);
    void enterState(SwarmMode swarmMode, AtmosphereMode atmoMode, float lerpSpeed);

    void enterPostprocessState(PostProcessMode postMode, float lerpSpeed);
};
}
