#include "Hive.hh"

#include <algorithm>

#include <glow-extras/colors/color.hh>
#include <glow-extras/geometry/Cube.hh>
#include <glow-extras/geometry/UVSphere.hh>
#include <glow-extras/pipeline/lights/Light.hh>

#include "../../particles/Emitter.hh"
#include "../../particles/PrimitiveParticleRenderer.hh"
#include "../../swarm/SwarmlingC.hh"
#include "../LightC.hh"
#include "../MaterialC.hh"
#include "../MeshC.hh"
#include "../OneshotTimerC.hh"
#include "../RandomTravelAnimC.hh"
#include "../RotateAnimC.hh"
#include "../TransformC.hh"

void flock::Hive::spawnBoids(const glm::vec3 &position, unsigned amount)
{
    static auto const lightColor = glow::colors::color::from_hex("BD3071").to_rgb() * 15.f;

    // Constrain boid amount
    if (mBoidsAlive + amount > maxBoids)
        amount = maxBoids - maxBoids;

    for (auto i = 0u; i < amount; ++i)
    {
        auto const randomOffset = glm::vec3(mFloatRange(mRng), mFloatRange(mRng), mFloatRange(mRng)) * boidPositionSpreadRange;
        auto const spawnPos = position + randomOffset;

        bool const isGlowSkull = (i % 10 == 0);

        auto const boidEntity = mRegistry->create();
        mRegistry->assign<TransformC>(boidEntity, glow::transform(spawnPos));
        mRegistry->assign<SwarmlingC>(boidEntity, spawnPos);
        mRegistry->assign<InstancedMeshC>(boidEntity, mInstancingManager->createInstance(mBoidMesh, mBoidMaterial));
        mRegistry->assign<SensorC>(boidEntity, mPhysicsEngine->createSensor(boidEntity, spawnPos, glm::vec3(1.5f)));

        if (isGlowSkull)
        {
            mRegistry->assign<HiveHealthC>(boidEntity, 2.f, HiveLifeform::GlowSkull);
            mRegistry->assign<LightC>(boidEntity, std::make_shared<glow::pipeline::Light>(glm::vec3(0), lightColor, 2.f, 10.f));
        }
        else
        {
            mRegistry->assign<HiveHealthC>(boidEntity, boidHealth, HiveLifeform::Skull);
        }
    }

    mBoidsAlive += amount;
}

void flock::Hive::spawnBlood(const glm::vec3 &position, unsigned amount)
{
    if (amount == 1)
    {
        mChalice->spawnBlood(position, mInstancingManager.get());
    }
    else
    {
        for (auto i = 0u; i < amount; ++i)
        {
            auto const randomOffset = glm::vec3(mFloatRange(mRng), mFloatRange(mRng), mFloatRange(mRng)) * boidPositionSpreadRange;
            auto const spawnPos = position + randomOffset;
            mChalice->spawnBlood(spawnPos, mInstancingManager.get());
        }
    }
}

void flock::Hive::spawnSquid(const glm::vec3 &position)
{
    if (mSquidsAlive >= maxSquids)
        return;

    ++mSquidsAlive;

    auto const spawnPos = getSafeSpawnPos(position);
    auto const squidEntity = mRegistry->create();
    mRegistry->assign<TransformC>(squidEntity, glow::transform(spawnPos, glm::quat{}, glm::vec3(4.f)));
    mRegistry->assign<HiveHealthC>(squidEntity, squidHealth, HiveLifeform::Squid);
    mRegistry->assign<RandomTravelAnimC>(squidEntity, 10.f);
    mRegistry->assign<InstancedMeshC>(squidEntity, mInstancingManager->createInstance(mSquidMesh, mSquidMaterial));
    mRegistry->assign<SensorC>(squidEntity, mPhysicsEngine->createSensor(squidEntity, spawnPos, glm::vec3(4.f)));
    mRegistry->assign<RotateAnimC>(squidEntity, 50 * (mSquidsAlive % 2 == 0 ? 1 : -1));
    // static auto const lightColor = glow::colors::color::from_hex("FF9E24").to_rgb() * 50.f;
    // mRegistry->assign<LightC>(squidEntity, std::make_shared<glow::pipeline::Light>(position, lightColor, 15.f));
    mRegistry->assign<HiveSpawnerC>(squidEntity, 5.f, 15);
}

void flock::Hive::spawnTerrorSquid(const glm::vec3 &position)
{
    ++mTerrorSquidsAlive;

    auto const spawnPos = getSafeSpawnPos(position);
    auto const squidEntity = mRegistry->create();
    mRegistry->assign<TransformC>(squidEntity, glow::transform(spawnPos, glm::quat{}, glm::vec3(4.f)));
    mRegistry->assign<HiveHealthC>(squidEntity, terrorSquidHealth, HiveLifeform::TerrorSquid);
    mRegistry->assign<RandomTravelAnimC>(squidEntity, 30.f);
    mRegistry->assign<MeshC>(squidEntity, mTerrorSquidMesh);
    mRegistry->assign<MaterialC>(squidEntity, mTerrorSquidMaterial);
    mRegistry->assign<SensorC>(squidEntity, mPhysicsEngine->createSensor(squidEntity, spawnPos, glm::vec3(4.f)));
    mRegistry->assign<RotateAnimC>(squidEntity, 150);
    mRegistry->assign<EmitterC>(squidEntity, glow::colors::color::from_hex("FF3333").to_rgb());
    // static auto const lightColor = glow::colors::color::from_hex("FF9E24").to_rgb() * 50.f;
    // mRegistry->assign<LightC>(squidEntity, std::make_shared<glow::pipeline::Light>(position, lightColor, 15.f));
    mRegistry->assign<HiveSpawnerC>(squidEntity, .25f, 3);
}

void flock::Hive::onSpawnTerrorSquid(const flock::GameEvents::SpawnTerrorSquids &event)
{
    for (auto i = 0u; i < event.amount; ++i)
    {
        auto const randomPosition
            = glm::vec3(mFloatRange(mRng), mFloatRange(mRng), mFloatRange(mRng)) * glm::vec3(150.f, 10.f, 150.f);
        spawnTerrorSquid(randomPosition);
    }
}

void flock::Hive::onSpawnSquid(const flock::GameEvents::SpawnSquids &event)
{
    for (auto i = 0u; i < event.amount; ++i)
    {
        auto const randomPosition
            = glm::vec3(mFloatRange(mRng), mFloatRange(mRng), mFloatRange(mRng)) * glm::vec3(150.f, 10.f, 150.f);
        spawnSquid(randomPosition);
    }
}

glm::vec3 flock::Hive::getSafeSpawnPos(const glm::vec3 &pos)
{
    auto const playerDistance2 = glm::distance2(pos, mCurrentPlayerPosition);
    if (playerDistance2 < playerSafetySphere2)
    {
        return mCurrentPlayerPosition + glm::vec3(playerSafetySphere / 2.f, -playerSafetySphere, playerSafetySphere / 2.f);
    }
    else
    {
        return pos;
    }
}

void flock::Hive::setChalice(flock::Chalice *chalice)
{
    mChalice = chalice;
}

flock::Hive::Hive(entt::registry<uint32_t> *reg,
                  PhysicsEngine *phys,
                  const std::shared_ptr<flock::InstancingManager> &instMan,
                  GameEvents *events,
                  const glow::SharedVertexArray &boidMesh,
                  glow::SharedVertexArray const &squidMesh,
                  glow::SharedVertexArray const &terrorSquidMesh)
  : mRegistry(reg),
    mPhysicsEngine(phys),
    mInstancingManager(instMan),
    mEvents(events),
    mBoidMesh(boidMesh),
    mSquidMesh(squidMesh),
    mTerrorSquidMesh(terrorSquidMesh)
{
    mEvents->dispatcher().sink<GameEvents::SpawnSquids>().connect<&Hive::onSpawnSquid>(this);
    mEvents->dispatcher().sink<GameEvents::SpawnTerrorSquids>().connect<&Hive::onSpawnTerrorSquid>(this);
}

void flock::Hive::onHitEntity(uint32_t ent, float damage)
{
    HiveHealthC &hiveHealth = mRegistry->get<HiveHealthC>(ent);
    if (hiveHealth.applyDamage(damage))
    {
        auto const &squidPos = mRegistry->get<TransformC>(ent).transform.position;
        auto const lifeform = hiveHealth.getLifeform();

        if (lifeform == HiveLifeform::Skull || lifeform == HiveLifeform::GlowSkull)
        {
            --mBoidsAlive;

            if (lifeform == HiveLifeform::GlowSkull)
                spawnBlood(squidPos, 1);
        }
        else if (lifeform == HiveLifeform::Squid)
        {
            --mSquidsAlive;

            spawnBlood(squidPos, 2);
        }
        else if (lifeform == HiveLifeform::TerrorSquid)
        {
            --mTerrorSquidsAlive;

            if (mTerrorSquidsAlive == 0)
                mEvents->trigger<GameEvents::BossDeath>();

            spawnBlood(squidPos, 20);
        }

        // Squid Explosion
        if (lifeform == HiveLifeform::Squid || lifeform == HiveLifeform::TerrorSquid)
        {
            static auto const explosionDuration = .15f;

            auto const explosionEnt = mRegistry->create();
            mRegistry->assign<SphereParticleC>(
                explosionEnt, SphereParticleC(squidPos, 4.f, glm::vec4(1.5f * glm::vec3(1.f, .75f, .4f), .5f), explosionDuration));
            mRegistry->assign<OneshotTimerC>(explosionEnt, explosionDuration);
        }

        mRegistry->destroy(ent);
    }
}

void flock::Hive::tick(float dt, glm::vec3 const &playerPos)
{
    mCurrentPlayerPosition = playerPos;

    auto spawnerView = mRegistry->view<TransformC, HiveSpawnerC>();
    std::for_each(spawnerView.begin(), spawnerView.end(), [&](auto const ent) {
        auto &spawner = mRegistry->get<HiveSpawnerC>(ent);
        if (spawner.tick(dt))
        {
            auto &transform = mRegistry->get<TransformC>(ent);
            spawnBoids(transform.transform.position, spawner.getSpawnAmount());
        }
    });
}
