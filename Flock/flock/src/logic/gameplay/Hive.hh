#pragma once

#include <algorithm>
#include <iostream>
#include <memory>
#include <random>

#include <entt/entity/registry.hpp>

#include <glm/glm.hpp>

#include <glow/objects/VertexArray.hh>

#include <glow-extras/colors/color.hh>

#include "../../instancing/InstancingManager.hh"
#include "../physics/PhysicsEngine.hh"
#include "Chalice.hh"

/**
 * The Hive
 *
 * Manages the swarm of enemies, the monkey heads and spawner spheres
 * Receives onHit events by the player controller
 * Receives onSpawn.. events by the game controller
 */

namespace flock
{
enum class HiveLifeform
{
    Skull,      // Monkey Boid
    GlowSkull,  // Monkey Boid with sphere light
    Squid,      // Black Sphere
    TerrorSquid // Red Sphere
};

struct HiveSpawnerC
{
private:
    static constexpr auto firstSpawnDelay = .15f;

    float mSpawnInterval = 5.f;
    unsigned mSpawnAmount = 20;

    float mTimeToNextSpawn = firstSpawnDelay;
    bool mActive = true;

public:
    HiveSpawnerC(float spawnInterval = 10.f, unsigned spawnAmount = 20)
      : mSpawnInterval(spawnInterval), mSpawnAmount(spawnAmount)
    {
    }

    bool tick(float dt)
    {
        if (!mActive)
            return false;

        mTimeToNextSpawn -= dt;

        if (mTimeToNextSpawn <= 0.f)
        {
            mTimeToNextSpawn = mSpawnInterval;
            return true;
        }
        else
        {
            return false;
        }
    }

    constexpr unsigned getSpawnAmount() const noexcept { return mSpawnAmount; }
    constexpr bool isActive() const noexcept { return mActive; }
};

struct HiveHealthC
{
private:
    float mCurrentHealth;
    HiveLifeform mLifeform;

public:
    HiveHealthC(float maxHealth, HiveLifeform lifeform = HiveLifeform::Skull)
      : mCurrentHealth(maxHealth), mLifeform(lifeform)
    {
    }

    /// Apply the given amount of damage
    /// Returns true if dead
    bool applyDamage(float amount)
    {
        mCurrentHealth -= amount;
        if (mCurrentHealth <= 0)
            return true;
        return false;
    }

    constexpr HiveLifeform getLifeform() const noexcept { return mLifeform; }
};

// Controls the swarm
class Hive
{
private:
    static constexpr auto maxBoids = 1500; ///< Maximum amount of boids concurrently alive
    static constexpr auto maxSquids = 50;  ///< Maximum amount of spawner spheres concurrently alive
    static constexpr auto boidPositionSpreadRange
        = 1.5f; ///< Half-size in meters of the cube surrounding the spawners, in which boids are spawned randomly

    static constexpr auto boidHealth = 1.f;   ///< Health of boids (1 hit is 1 damage)
    static constexpr auto squidHealth = 10.f; ///< Health of spawner spheres ("squids")
    static constexpr auto terrorSquidHealth = 150.f;

    static constexpr auto playerSafetySphere = 35.f; ///< Radius of the sphere around the player in which no squids are spawned, in meters
    static constexpr auto playerSafetySphere2 = playerSafetySphere * playerSafetySphere;

private:
    std::mt19937 mRng{};
    std::uniform_real_distribution<float> mFloatRange{-1.f, 1.f};

    entt::registry<uint32_t>* const mRegistry;
    PhysicsEngine* const mPhysicsEngine;
    std::shared_ptr<InstancingManager> const mInstancingManager;
    GameEvents* const mEvents;
    Chalice* mChalice = nullptr;

    glow::SharedVertexArray const mBoidMesh;
    MaterialC const mBoidMaterial = MaterialC{glow::colors::color::from_hex("E5736E").to_rgb(), .3f, .8f};

    glow::SharedVertexArray const mSquidMesh;
    MaterialC const mSquidMaterial = MaterialC{glow::colors::color::from_hex("333333").to_rgb(), 1.f, .5f};

    glow::SharedVertexArray const mTerrorSquidMesh;
    MaterialC const mTerrorSquidMaterial = MaterialC{glow::colors::color::from_hex("FF3333").to_rgb(), 0.25f, .0f};

    unsigned mBoidsAlive = 0;        ///< Number of boids currently alive
    unsigned mSquidsAlive = 0;       ///< Number of squids currently alive
    unsigned mTerrorSquidsAlive = 0; ///< Number of terror squids currently alive

    glm::vec3 mCurrentPlayerPosition{0};

private:
    void spawnBoids(glm::vec3 const& position, unsigned amount);
    void spawnBlood(glm::vec3 const& position, unsigned amount);

    void spawnSquid(glm::vec3 const& position);
    void spawnTerrorSquid(glm::vec3 const& position);

    void onSpawnTerrorSquid(GameEvents::SpawnTerrorSquids const&);
    void onSpawnSquid(GameEvents::SpawnSquids const&);

    glm::vec3 getSafeSpawnPos(glm::vec3 const& pos);

public:
    Hive(entt::registry<uint32_t>* reg,
         PhysicsEngine* phys,
         std::shared_ptr<InstancingManager> const& instMan,
         GameEvents* events,
         glow::SharedVertexArray const& boidMesh,
         glow::SharedVertexArray const& squidMesh,
         glow::SharedVertexArray const& terrorSquidMesh);

    void setChalice(Chalice* chalice);

    void onHitEntity(uint32_t ent, float damage = 1.f);

    void tick(float dt, glm::vec3 const& playerPos);
};
}
