#include "PlayerController.hh"

void flock::PlayerController::createPlayerEntity(const glow::SharedVertexArray &playerMesh)
{
    auto const initialRotation = glm::quat();

    mEntityPlayer = mRegistry->create();
    mRegistry->assign<TransformC>(mEntityPlayer, glow::transform(glm::vec3(0), initialRotation, glm::vec3(.35f)));
    mRegistry->assign<LightC>(mEntityPlayer, std::make_shared<glow::pipeline::Light>(glm::vec3(0), mLaserColor, 1.5f, 25.f));
    mRegistry->assign<SwarmlingTargetC>(mEntityPlayer);

    mRegistry->assign<MeshC>(mEntityPlayer, playerMesh);
    mRegistry->assign<MaterialC>(mEntityPlayer, glow::colors::color::from_hex("777777").to_rgb(), .75f, 1.f);
    mRegistry->assign<EmitterC>(mEntityPlayer, glow::colors::color::from_hex("29B9E8").to_rgb() * .05f);
    // mRegistry->assign<SensorC>(mPlayerEntity, mPhysicsEngine.createSensor(mPlayerEntity, startPos, glm::vec3(1, 1, 1)));

    mPhysicalRotation = mTargetRotation = initialRotation;
}

void flock::PlayerController::applyWeaponUpgrade(unsigned level)
{
    switch (level)
    {
    case 0:
    {
        auto const beam = mRegistry->create();
        mRegistry->assign<LineParticleC>(beam, LineParticleC{glm::vec3(0), glm::vec3(0, 0, 50), glm::vec4(mLaserColor, 1)});

        mWeapons.emplace_back(Weapon(beam, 0.f, glm::vec3(0, 0, 0)));
        break;
    }
    case 1:
    {
        for (auto const &weaponOffset : {glm::vec3(-.75f, -.35f, 0), glm::vec3(.75f, -.35f, 0)})
        {
            auto const beam = mRegistry->create();
            mRegistry->assign<LineParticleC>(
                beam, LineParticleC{glm::vec3(0), glm::vec3(0, 0, 50), glm::vec4(glm::vec3(.8f, .1f, .1f), 1)});

            mWeapons.emplace_back(Weapon(beam, weaponCadence * 2, weaponOffset,
                                         glm::normalize(glm::vec3{weaponOffset.x * .01f, 0, -1.f})));
        }

        MaterialC &playerMat = mRegistry->get<MaterialC>(mEntityPlayer);
        playerMat.roughness = .6f;
        playerMat.albedo = glow::colors::color::from_hex("557799").to_rgb();

        break;
    }
    case 2:
    {
        auto const xOff = 3.f;
        auto const yOff = .5f;
        auto const zOff = 2.5f;
        auto const xSpread = .005f;
        auto const ySpread = .03f;

        for (auto const &weaponOffset : {glm::vec3(-xOff, yOff, zOff), glm::vec3(xOff, yOff, zOff),
                                         glm::vec3(-xOff, -yOff, zOff), glm::vec3(xOff, -yOff, zOff)})
        {
            auto const beam = mRegistry->create();
            mRegistry->assign<LineParticleC>(
                beam, LineParticleC{glm::vec3(0), glm::vec3(0, 0, 50), glm::vec4(glm::vec3(.5f, 1.f, .25f), 1)});

            mWeapons.emplace_back(Weapon(beam, weaponCadence * 3.5f, weaponOffset * glm::vec3(1, .25f, 1),
                                         glm::normalize(glm::vec3{weaponOffset.x * xSpread, weaponOffset.y * ySpread, -1.f})));
        }

        MaterialC &playerMat = mRegistry->get<MaterialC>(mEntityPlayer);
        playerMat.roughness = .4f;
        playerMat.albedo = glow::colors::color::from_hex("2277BB").to_rgb();

        break;
    }
    case 3:
    {
        auto const xOff = .75f;
        auto const yOff = .75f;
        auto const zOff = 1.5f;
        auto const xSpread = .03f;
        auto const ySpread = .03f;

        for (auto const &weaponOffset : {glm::vec3(-xOff, yOff, zOff), glm::vec3(xOff, yOff, zOff),
                                         glm::vec3(-xOff, -yOff, zOff), glm::vec3(xOff, -yOff, zOff)})
        {
            auto const beam = mRegistry->create();
            mRegistry->assign<LineParticleC>(beam, LineParticleC{glm::vec3(0), glm::vec3(0, 0, 50),
                                                                 glm::vec4(glow::colors::color::from_hex("d13c9f").to_rgb(), 1)});

            mWeapons.emplace_back(Weapon(beam, weaponCadence * 1.5f, weaponOffset * glm::vec3(1, .25f, 1),
                                         glm::normalize(glm::vec3{weaponOffset.x * xSpread, weaponOffset.y * ySpread, -1.f})));
        }

        MaterialC &playerMat = mRegistry->get<MaterialC>(mEntityPlayer);
        playerMat.roughness = .25f;
        playerMat.albedo = glow::colors::color::from_hex("d13c9f").to_rgb();

        break;
    }
    case 4:
    {
        auto const yOff = -.75f;
        auto const zOff = 3.f;
        auto const xSpread = .01f;
        auto const ySpread = .03f;

        for (auto xOff : {-2.5f, -2.f, -1.5f, 1.5f, 2.f, 2.5f})
        {
            auto const weaponOffset = glm::vec3(xOff, yOff, zOff);
            auto const beam = mRegistry->create();
            mRegistry->assign<LineParticleC>(beam, LineParticleC{glm::vec3(0), glm::vec3(0, 0, 50),
                                                                 glm::vec4(glow::colors::color::from_hex("4B719F").to_rgb(), 1)});

            mWeapons.emplace_back(Weapon(beam, weaponCadence * 1.5f, weaponOffset * glm::vec3(1, .25f, 1),
                                         glm::normalize(glm::vec3{weaponOffset.x * xSpread, weaponOffset.y * ySpread, -1.f})));
        }

        MaterialC &playerMat = mRegistry->get<MaterialC>(mEntityPlayer);
        playerMat.roughness = .15f;
        playerMat.albedo = glow::colors::color::from_hex("4B719F").to_rgb();

        break;
    }
    default:
        std::cerr << "Invalid weapon upgrade level " << level << std::endl;
        break;
    }
}

void flock::PlayerController::updateWeapon(float dt,
                                           flock::PlayerController::Weapon &weapon,
                                           bool fireInput,
                                           const glow::transform &playerTransform,
                                           flock::PhysicsEngine &physics)
{
    if (weapon.active)
    {
        // Weapon still firing

        weapon.activeTimer += dt;
        if (weapon.activeTimer >= weaponFireDuration)
        {
            // Firing interval over, start cooldown
            weapon.active = false;
            weapon.hasHit = false;
            weapon.cooldown = weaponCadence;
        }
    }
    else
    {
        if (weapon.firstShot && fireInput)
        {
            weapon.cooldown = weapon.idleTriggerOffset;
            weapon.firstShot = false;
        }

        weapon.cooldown -= dt;

        if (weapon.cooldown <= 0.f)
        {
            // Weapon ready and not firing
            if (fireInput)
            {
                weapon.activeTimer = 0.f;
                weapon.active = true;
            }
            else
            {
                weapon.firstShot = true;
            }
        }
    }

    LineParticleC &fireBeam = mRegistry->get<LineParticleC>(weapon.entityLaserBeam);

    if (weapon.active)
    {
        auto const fireOrigin = playerTransform.getRelativePosition(mFireOffset + weapon.weaponOffset);
        auto const fireDestination
            = playerTransform.getRelativePosition((weapon.weaponForward * playerWeaponRange) + weapon.weaponOffset);
        auto const fireDirection = fireDestination - fireOrigin;

        float hitDistance = playerWeaponRange;
        bool hitThisFrame = false;

        if (!weapon.hasHit)
        {
            auto target = physics.castRay(fireOrigin, fireDirection, playerWeaponRange, &hitDistance);
            if (target)
            {
                // Hit
                mHive->onHitEntity(target.value());
                onHit();
                weapon.hasHit = true;
                hitThisFrame = true;
            }
        }

        fireBeam.active = true;
        fireBeam.setModelMatrix(fireOrigin, fireDestination);

        if (hitThisFrame)
        {
            auto const hitPosition = playerTransform.getRelativePosition(weapon.weaponForward * hitDistance);

            auto const explosion = mRegistry->create();
            mRegistry->assign<StaticEmitterC>(
                explosion, StaticEmitterC{mExplosionColor, hitPosition, (mPhysicalRotation * weapon.weaponForward) * -20.f});
            mRegistry->assign<OneshotTimerC>(explosion, .2f);
        }
    }
    else
    {
        // No fire, distable beam
        fireBeam.active = false;
    }
}

void flock::PlayerController::onHit()
{
    mMultiplierTimer = comboTime;

    ++mMultiplierStep;
    if (mMultiplierStep >= multiplierStep && mMultiplier < maxMultiplier)
    {
        mMultiplier += .2f;
        mMultiplierStep = 0;
    }

    mScore += 10.f * mMultiplier;
}

void flock::PlayerController::onDeath(const glm::vec3 &deathPosition)
{
    // Spawn explosion
    for (auto const &direction : {glow::transform::Forward(), glow::transform::Up(), glow::transform::Right()})
        for (auto sign : {1, -1})
        {
            auto const explosion = mRegistry->create();
            mRegistry->assign<StaticEmitterC>(
                explosion, StaticEmitterC{mExplosionColor, deathPosition, (mPhysicalRotation * (direction * sign)) * -25.f});
            mRegistry->assign<OneshotTimerC>(explosion, .75f);
        }

    // Kill player
    mAlive = false;
    mRegistry->destroy(mEntityPlayer);

    // Kill weapons
    for (auto const &weapon : mWeapons)
    {
        mRegistry->destroy(weapon.entityLaserBeam);
    }
}

void flock::PlayerController::updateScore(float dt)
{
    if (mMultiplier > 1.f)
    {
        mMultiplierTimer -= dt;
        if (mMultiplierTimer <= 0.f)
        {
            mMultiplierTimer = comboTime;
            mMultiplier = 1.f;
        }
    }
}

flock::PlayerController::PlayerController(entt::registry<uint32_t> *reg, flock::Hive *hive, GameEvents *events, const glow::SharedVertexArray &playerMesh)
  : mRegistry(reg), mHive(hive), mEvents(events)
{
    createPlayerEntity(playerMesh);

    applyWeaponUpgrade(mWeaponUpgradeLevel);

    mEvents->dispatcher().sink<GameEvents::BloodPickup>().connect<&PlayerController::onReceiveBlood>(this);

    mBloodLeftToUpgrade = upgradeBloodRequirements[mWeaponUpgradeLevel];
}

void flock::PlayerController::update(float dt, flock::PlayerController::PlayerInput &&input, flock::PhysicsEngine &physics)
{
    if (!mAlive)
    {
        mCamShakeIntensity = 0.f;
        return;
    }

    glow::transform &playerTransform = mRegistry->get<TransformC>(mEntityPlayer).transform;

    // Collision check
    {
        auto const centerPos = playerTransform.getRelativePosition(mShipCenterOffset);
        if (physics.isPositionOverlapped(centerPos))
        {
            onDeath(centerPos);
            return;
        }
    }

    // Positioning
    {
        // Controller Rotation
        {
            if (input.turnControl != 0.f)
                mTargetRotation *= glm::quat(glm::vec3(0, glm::radians(-input.turnControl * mControllerRotationSpeed * dt), 0));
        }

        auto const mouseDeltaCap = maxMouseAmplitude * dt;
        mLastMouseYDelta = std::clamp(mLastMouseYDelta, -mouseDeltaCap, mouseDeltaCap);

        mVelocity *= std::pow(mVelocityDamping, dt);

        bool const boosting = !input.fireDown && input.boostDown; // Only allow boosting while not firing

        glm::vec3 directionalAcceleration = input.directionalInput * (mEngineSpeed * dt * (boosting ? mBoostMultiplier : 1.f));

        // Reverse penalty
        if (directionalAcceleration.z > 0)
            directionalAcceleration.z *= reversePenalty;

        glm::vec3 totalAcceleration = playerTransform.rotation * directionalAcceleration
                                      + (glow::transform::Up() * (-mLastMouseYDelta * mEngineSpeed));
        mVelocity += totalAcceleration;

        mPhysicalRotation = glm::slerp(mPhysicalRotation, mTargetRotation, std::min(dt * mRotationSensitivity, 1.f));
        playerTransform.rotation = mPhysicalRotation;
        playerTransform.position += mVelocity * dt;

        // Level limits
        playerTransform.position = glm::clamp(playerTransform.position, mPlayingFieldMin, mPlayingFieldMax);

        // Particle velocity
        EmitterC &engineEmitter = mRegistry->get<EmitterC>(mEntityPlayer);
        engineEmitter.velocity = mVelocity;

        mPosition = playerTransform.position;
        mLastMouseYDelta = 0.f;

        // Cam Shake
        {
            if (boosting && glm::length2(input.directionalInput) > 0.1f)
                mCamShakeIntensity = glm::lerp(mCamShakeIntensity, 1.f, dt * 3.5f);
            else
                mCamShakeIntensity = glm::lerp(mCamShakeIntensity, 0.f, dt * 2.5f);
        }
    }

    // Camera
    {
        mTargetCamPos = playerTransform.getRelativePosition(glm::vec3(0, 3, 15));
        mTargetCamFocus = playerTransform.getRelativePosition({0, 0, -50});
    }

    // Weapons
    {
        mFiring = input.fireDown;

        for (auto &weapon : mWeapons)
        {
            updateWeapon(dt, weapon, input.fireDown, playerTransform, physics);
        }

        if (input.fireDown)
        {
            mWeaponLightIntensity = glm::lerp(mWeaponLightIntensity, weaponLightIntensityMax, dt * 1.5f);
        }
        else
        {
            mWeaponLightIntensity = glm::lerp(mWeaponLightIntensity, 0.f, dt * 2.5f);
        }

        LightC &fireLight = mRegistry->get<LightC>(mEntityPlayer);
        fireLight.light->setColor(mWeaponLightColor * mWeaponLightIntensity);

        updateScore(dt);
    }
}

void flock::PlayerController::applyMouse(float dX, float dY)
{
    if (!mAlive)
        return;

    mTargetRotation *= glm::quat(glm::vec3(/*glm::radians(dY * mRotationSpeed)*/ 0, glm::radians(-dX * mRotationSpeed), 0));
    mLastMouseYDelta += dY;

    //        auto altitude = glm::atan(mTargetForwardVector.y, length(glm::vec2(mTargetForwardVector.x,
    //        mTargetForwardVector.z))); auto azimuth = glm::atan(mTargetForwardVector.z, mTargetForwardVector.x);

    //        azimuth -= dX * mRotationSpeed;
    //        altitude = glm::clamp(altitude - dY * mRotationSpeed, -0.499f * glm::pi<float>(), 0.499f * glm::pi<float>());

    //        auto const caz = glm::cos(azimuth);
    //        auto const saz = glm::sin(azimuth);
    //        auto const cal = glm::cos(altitude);
    //        auto const sal = glm::sin(altitude);

    //        mTargetForwardVector = glm::vec3(cal * caz, sal, cal * saz);
    //        glm::vec3 const right = normalize(cross(mTargetForwardVector, glm::vec3(0, 1, 0)));
    //        glm::vec3 const up = cross(right, mTargetForwardVector);
    //        mTargetRotation = glm::quat_cast(transpose(glm::mat3(right, up, -mTargetForwardVector)));
}

void flock::PlayerController::onReceiveBlood(GameEvents::BloodPickup const &)
{
    ++mCollectedBlood;

    if (mWeaponUpgradeLevel < upgradeBloodRequirements.size())
    {
        // Next upgrade is available
        --mBloodLeftToUpgrade;

        if (mCollectedBlood >= upgradeBloodRequirements[mWeaponUpgradeLevel])
        {
            // Upgrade
            onWeaponUpgrade();

            if (mWeaponUpgradeLevel < upgradeBloodRequirements.size())
            {
                mBloodLeftToUpgrade = upgradeBloodRequirements[mWeaponUpgradeLevel] - mCollectedBlood;
            }
            else
            {
                // Reached final upgrade
                mNextUpgradeAvailable = false;
            }
        }
    }
}

void flock::PlayerController::onWeaponUpgrade()
{
    if (mWeaponUpgradeLevel < maxWeaponUpgradeLevel)
    {
        ++mWeaponUpgradeLevel;
        applyWeaponUpgrade(mWeaponUpgradeLevel);
        mEvents->trigger<GameEvents::WeaponUpgrade>();
    }
}
