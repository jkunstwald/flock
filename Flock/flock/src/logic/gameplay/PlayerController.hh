#pragma once

#include <algorithm>
#include <array>

#include <glm/ext.hpp>
#include <glm/glm.hpp>

#include <glow/math/transform.hh>

#include <glow-extras/colors/color.hh>
#include <glow-extras/pipeline/lights/Light.hh>

#include "../../particles/Emitter.hh"
#include "../../particles/PrimitiveParticleRenderer.hh"
#include "../../swarm/SwarmlingC.hh"
#include "../LightC.hh"
#include "../MaterialC.hh"
#include "../MeshC.hh"
#include "../OneshotTimerC.hh"
#include "../TransformC.hh"
#include "../physics/PhysicsEngine.hh"
#include "GameEvents.hh"
#include "Hive.hh"

namespace flock
{
class PlayerController
{
public:
    struct PlayerInput
    {
        glm::vec3 directionalInput;
        bool fireDown;
        bool boostDown;
        float turnControl;
    };

private:
    static constexpr auto maxWeaponUpgradeLevel = 4;
    static constexpr std::array upgradeBloodRequirements = {40u, 200u, 500u, 1000u};

    static constexpr auto weaponCadence = .03f;
    static constexpr auto weaponFireDuration = .03f;
    static constexpr auto weaponLightIntensityMax = 7.f;
    static constexpr auto playerWeaponRange = 300.f;

    static constexpr auto comboTime = 3.f;     ///< Amount of time the multiplier lingers without kills
    static constexpr auto multiplierStep = 40; ///< Amount of hits to reach a higher multiplier step
    static constexpr auto maxMultiplier = 2.f; ///< The maximal multiplier

    static constexpr auto maxMouseAmplitude = 3.f; ///< Amount of distance the ship can move vertically based on mouse movement, in m/s

    static constexpr auto reversePenalty = 0.4f; ///< Amount of speed the ship moves when reversing, in percent

    struct Weapon
    {
        bool active = false;             ///< Whether the weapon is actively firing
        bool hasHit = false;             ///< Whether the current shot has hit (and impacted) a target
        bool firstShot = true;           ///< Whether this is the first shot after being ready
        float activeTimer = 0.f;         ///< Amount of time the weapon has been actively firing
        float cooldown = 0.f;            ///< Amount of time the weapon requires to cooldown before the next shot
        float idleTriggerOffset = 0.f;   ///< Amount of time the weapon waits until firing if initially ready
        glm::vec3 const weaponOffset{0}; ///< The position offset of the weapon
        uint32_t const entityLaserBeam;  ///< Entity of the weapon's laser beam
        glm::vec3 const weaponForward;   ///< The forward direction of the weapon


        Weapon(uint32_t laserBeam, float triggerOffset, glm::vec3 const& offset, glm::vec3 const& forward = glow::transform::Forward())
          : idleTriggerOffset(triggerOffset), weaponOffset(offset), entityLaserBeam(laserBeam), weaponForward(forward)
        {
        }
    };

private:
    entt::registry<uint32_t>* const mRegistry;
    Hive* const mHive;
    GameEvents* const mEvents;

    // == Rotation ==
    glm::quat mPhysicalRotation{};
    glm::quat mTargetRotation{};

    float mRotationSensitivity = 30.f;
    float mRotationSpeed = 60.f;
    float mControllerRotationSpeed = 80.f;

    // == Position ==
    glm::vec3 mPosition{0};
    glm::vec3 mVelocity{0};
    float mVelocityDamping = .05f;

    // == Player Entity ==
    uint32_t mEntityPlayer;

    // == State ==
    float mWeaponLightIntensity = 0.f;
    float mCamShakeIntensity = 0.f;

    unsigned mCollectedBlood = 0;

    std::vector<Weapon> mWeapons;
    float mLastMouseYDelta = 0.01f; // If exactly 0 this causes a rare crash when holding nothing but S after game launch
    bool mAlive = true;
    bool mFiring = false;

    glm::vec3 mTargetCamPos;
    glm::vec3 mTargetCamFocus;

    // == Config ==
    glm::vec3 const mShipCenterOffset{0, -0.28f, -1.85f};
    glm::vec3 const mFireOffset{0, -0.28f, -3.33f};
    glm::vec3 const mWeaponLightColor{1, 0, 0};
    glm::vec3 const mLaserColor = glm::vec3{1, .75f, .05f};
    glm::vec3 const mExplosionColor = glm::vec3{1, .25f, .05f};

    glm::vec3 const mPlayingFieldMin = glm::vec3{-300.f, -300.f, -300.f};
    glm::vec3 const mPlayingFieldMax = glm::vec3{300.f, 300.f, 300.f};
    float const mEngineSpeed = 100.f;
    float const mBoostMultiplier = 1.95f;

    // == ImGui read-only ==
public:
    unsigned mWeaponUpgradeLevel = 0;

    float mScore = 0;
    float mMultiplier = 1.f;
    unsigned mBloodLeftToUpgrade = 0;
    bool mNextUpgradeAvailable = true;

private:
    float mMultiplierTimer = 0.f;
    unsigned mMultiplierStep = 0;

private:
    void createPlayerEntity(glow::SharedVertexArray const& playerMesh);

    void applyWeaponUpgrade(unsigned level);
    void onWeaponUpgrade();

    void updateWeapon(float dt, Weapon& weapon, bool fireInput, glow::transform const& playerTransform, PhysicsEngine& physics);

    void onHit();
    void onDeath(glm::vec3 const& deathPosition);
    void onReceiveBlood(GameEvents::BloodPickup const& event);

    void updateScore(float dt);

public:
    PlayerController(entt::registry<uint32_t>* reg, Hive* hive, GameEvents* events, glow::SharedVertexArray const& playerMesh);

    void update(float dt, PlayerInput&& input, PhysicsEngine& physics);

    void applyMouse(float dX, float dY);

    constexpr glm::vec3 const& getTargetCamPos() const noexcept { return mTargetCamPos; }
    constexpr glm::vec3 const& getTargetCamFocus() const noexcept { return mTargetCamFocus; }
    constexpr bool isPlayerAlive() const noexcept { return mAlive; }
    constexpr bool isFiring() const noexcept { return mFiring; }
    constexpr glm::vec3 const& getPosition() const noexcept { return mPosition; }
    constexpr float getCamShakeIntensity() const noexcept { return mCamShakeIntensity; }
};
}
