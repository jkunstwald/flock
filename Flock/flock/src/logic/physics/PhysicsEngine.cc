#include "PhysicsEngine.hh"

#include "PointQuery.hh"
#include "Raycast.hh"

void flock::PhysicsEngine::onSensorDestruction(entt::registry<uint32_t> &ecs, uint32_t entity)
{
    SensorC const &sensor = ecs.get<SensorC>(entity);
    mScene.RemoveBody(sensor.body);
}

flock::PhysicsEngine::PhysicsEngine(float timestep) : mScene(q3Scene{timestep}) {}

void flock::PhysicsEngine::init(entt::registry<uint32_t> &reg)
{
    reg.destruction<SensorC>().connect<&PhysicsEngine::onSensorDestruction>(this);
}

void flock::PhysicsEngine::step()
{
    mScene.Step();
}

std::optional<uint32_t> flock::PhysicsEngine::castRay(const glm::vec3 &origin, const glm::vec3 &direction, float range, float *hitRange)
{
    physics::Raycast raycast;
    raycast.Init(q3Vec3{origin.x, origin.y, origin.z}, q3Vec3{direction.x, direction.y, direction.z}, range);
    mScene.RayCast(&raycast, raycast.data);

    if (raycast.impactBody)
    {
        if (hitRange)
            *hitRange = raycast.impactDistance;

        auto ent = static_cast<uint32_t>(reinterpret_cast<unsigned long>(raycast.impactBody->GetUserData()));
        return {ent};
    }
    else
    {
        return std::nullopt;
    }
}

bool flock::PhysicsEngine::isPositionOverlapped(const glm::vec3 &position)
{
    physics::PointQuery query;
    mScene.QueryPoint(&query, q3Vec3{position.x, position.y, position.z});
    return query.isTriggered();
}

void flock::PhysicsEngine::syncSensors(entt::registry<uint32_t> &reg)
{
    auto view = reg.view<SensorC, TransformC>();
    std::for_each(view.begin(), view.end(), [&](auto const ent) {
        SensorC &sensor = reg.get<SensorC>(ent);
        TransformC &transform = reg.get<TransformC>(ent);

        auto const &pos = transform.transform.position;
        sensor.body->SetTransform(q3Vec3{pos.x, pos.y, pos.z});
    });
}

flock::SensorC flock::PhysicsEngine::createSensor(uint32_t entity, const glm::vec3 &position, const glm::vec3 &size)
{
    return SensorC(entity, position, size, &mScene);
}
