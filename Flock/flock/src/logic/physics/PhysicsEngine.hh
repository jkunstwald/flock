#pragma once

#include <limits>
#include <memory>
#include <optional>
#include <vector>

#include <q3.h>

#include <glm/glm.hpp>

#include <entt/entity/registry.hpp>

#include "../TransformC.hh"
#include "SensorC.hh"

namespace flock
{
class PhysicsEngine
{
private:
    q3Scene mScene;

private:
    void onSensorDestruction(entt::registry<uint32_t>& ecs, uint32_t entity);

public:
    PhysicsEngine(float timestep);

    void init(entt::registry<uint32_t>& reg);

    void step();

    /// Casts a ray, returns the first hit sensors' entity id
    std::optional<uint32_t> castRay(glm::vec3 const& origin, glm::vec3 const& direction, float range = 10000.f, float* hitRange = nullptr);

    /// Checks if any sensors overlap the given position
    bool isPositionOverlapped(glm::vec3 const& position);

    /// Synchronizes the position of sensors within the physics engine to their transform components
    void syncSensors(entt::registry<uint32_t>& reg);

    SensorC createSensor(uint32_t entity, glm::vec3 const& position, glm::vec3 const& size);
};
}
