#pragma once

#include <q3.h>

namespace flock
{
namespace physics
{
class PointQuery : public q3QueryCallback
{
private:
    q3Body* mImpactBody = nullptr;

public:
    PointQuery() {}

    bool ReportShape(q3Box* shape) override
    {
        mImpactBody = shape->body;
        return true;
    }

    bool isTriggered() const { return !!mImpactBody; }
    constexpr q3Body* getBody() const noexcept { return mImpactBody; }
};

}
}
