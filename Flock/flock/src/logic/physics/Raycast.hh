#pragma once

#include <q3.h>

namespace flock
{
namespace physics
{
class Raycast : public q3QueryCallback
{
public:
    q3RaycastData data;
    r32 tfinal;

    r32 impactDistance;
    q3Vec3 impactNormal;
    q3Body* impactBody;

    bool ReportShape(q3Box* shape) override
    {
        if (data.toi < tfinal)
        {
            impactDistance = tfinal = data.toi;
            impactNormal = data.normal;
            impactBody = shape->body;
        }

        data.toi = tfinal;
        return true;
    }

    void Init(const q3Vec3& origin, const q3Vec3& dir, float range = 10000.f)
    {
        data.start = origin;
        data.dir = q3Normalize(dir);
        data.t = r32(range);
        tfinal = std::numeric_limits<float>().max();
        data.toi = data.t;
        impactBody = nullptr;
    }
};

}
}
