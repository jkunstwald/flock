#pragma once

#include <q3.h>

#include <glm/glm.hpp>

namespace flock
{
struct SensorC
{
    q3Body* body;

    SensorC(uint32_t ent, glm::vec3 const& position, glm::vec3 const& size, q3Scene* scene)
    {
        q3BodyDef bodyDef;
        bodyDef.position = q3Vec3(position.x, position.y, position.z);
        bodyDef.bodyType = eKinematicBody;
        // WARNING!! Do not change this without checking all occurences of GetUserData() in the project!
        bodyDef.userData = reinterpret_cast<void*>(ent);

        body = scene->CreateBody(bodyDef);

        q3BoxDef boxDef;
        q3Transform localSpace; // Identity Transform (0,0,0)
        q3Identity(localSpace);
        // boxDef.SetSensor(true);

        boxDef.Set(localSpace, q3Vec3(size.x, size.y, size.z));
        body->AddBox(boxDef);
    }
};
}
