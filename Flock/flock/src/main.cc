#include "FlockApp.hh"

#ifdef _WIN32
#include <Windows.h>
#endif

int main()
{
    // -- Windows only - hide console window --
#ifdef _WIN32
    ::ShowWindow(::GetConsoleWindow(), SW_HIDE);
#endif

    flock::FlockApp app;
    return app.run();
}
