#pragma once

#include <memory>
#include <vector>

#include <glm/vec3.hpp>

#include <glow/common/shared.hh>
#include <glow/objects/ArrayBuffer.hh>

namespace flock
{
struct Emitter
{
    glm::vec3 position = glm::vec3(0);
    glm::vec3 velocity = glm::vec3(0);
    glm::vec3 color = glm::vec3(1);
    //        unsigned packedColor = util::packColor(glm::vec3(0, 1, 0));

    Emitter() = default;
    Emitter(glm::vec3 const& pos, glm::vec3 const& velocity = glm::vec3(0), glm::vec3 const& color = glm::vec3(0, 1, 0))
      : position(pos), velocity(velocity), color(color)
    {
    }

    static std::vector<glow::ArrayBufferAttribute> Attributes()
    {
        return {{&Emitter::position, "aPosition"}, {&Emitter::velocity, "aVelocity"}, {&Emitter::color, "aColor"}};
    }
};

struct EmitterC
{
    glm::vec3 color = glm::vec3(1, 0, 0);
    glm::vec3 velocity = glm::vec3(0, 0, 0);
};

struct StaticEmitterC
{
    glm::vec3 color = glm::vec3(1, 0, 0);
    glm::vec3 position = glm::vec3(0);
    glm::vec3 velocity = glm::vec3(0, 1, 0);
    bool active = true;
};
}
