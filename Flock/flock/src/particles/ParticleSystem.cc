#include "ParticleSystem.hh"

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/TransformFeedback.hh>
#include <glow/objects/VertexArray.hh>

#include "../logic/TransformC.hh"

void flock::ParticleSystem::renderParticles(const glow::pipeline::RenderPipeline &pipeline,
                                            const glow::pipeline::RenderStage &stage,
                                            const glow::pipeline::CameraData &data)
{
    if (mState.systemEmpty)
        return;

    auto const currentIndex = mState.ring.end();

    {
        glm::vec4 const right = glm::row(data.view, 0);
        glm::vec4 const up = glm::row(data.view, 1);

        auto shader = mShaderBillboard->use();
        stage.prepareShader(pipeline, mShaderBillboard, shader);
        shader.setUniform("uMVP", data.vp);
        shader.setUniform("uMV", data.view);
        shader.setUniform("uRight", glm::vec3(right.x, right.y, right.z));
        shader.setUniform("uUp", glm::vec3(up.x, up.y, up.z));
        shader.setUniform("uBillboardSize", mConfig.billboardSize);
        shader.setUniform("uVelocityScale", mConfig.velocityScale);
        shader.setUniform("uParticleLifetime", mConfig.particleLifetime);
        shader.setTexture("uTexture", mTextureSpot);

        mParticleVaos[currentIndex]->bind().drawTransformFeedback(mParticleFeedbacks[currentIndex]);
    }
}

void flock::ParticleSystem::prepareEmitters()
{
    mEmitterBuffer->bind().setData(mEmitters);
}

void flock::ParticleSystem::emitParticles()
{
    if (mEmitters.empty())
        return;

    mState.systemEmpty = false;

    auto const integratedEmit = static_cast<int>(mState.deltaTime / mConfig.emitPeriod) * mConfig.emitCount;

    auto shader = mShaderEmitter->use();
    shader.setUniform("uEmitCount", integratedEmit);
    shader.setUniform("uDeltaTime", mState.deltaTime);
    shader.setUniform("uTime", mState.time);
    shader.setTexture("uRandomTexture", mTextureRandom);

    mEmitterVao->bind().negotiateBindings();

    {
        GLOW_SCOPED(enable, GL_RASTERIZER_DISCARD);

        auto feedback = mParticleFeedbacks[emittersIndex]->bind();

        feedback.begin(GL_POINTS);
        mEmitterVao->bind().draw();
        feedback.end();
    }
}

void flock::ParticleSystem::processParticles()
{
    if (mState.systemEmpty)
        return;

    auto const previousIndex = mState.ring.begin();
    auto const currentIndex = mState.ring.end();

    auto shader = mShaderFeedback->use();

    shader.setUniform("uParticleLifetime", mConfig.particleLifetime);
    shader.setUniform("uDeltaTime", mState.deltaTime);
    shader.setUniform("uTime", mState.time);
    shader.setUniform("uCTime", mState.time);
    shader.setTexture("uRandomTexture", mTextureRandom);

    mParticleVaos[emittersIndex]->bind().negotiateBindings();

    {
        GLOW_SCOPED(enable, GL_RASTERIZER_DISCARD);

        auto feedback = mParticleFeedbacks[currentIndex]->bind();

        feedback.begin(GL_POINTS);

        // Process particles that have been emitted this frame
        mParticleVaos[emittersIndex]->bind().drawTransformFeedback(mParticleFeedbacks[emittersIndex]);

        if (mState.ring.isFullCycle())
        {
            // Process particles from last frame
            mParticleVaos[previousIndex]->bind().drawTransformFeedback(mParticleFeedbacks[previousIndex]);
        }

        feedback.end();
    }
}

void flock::ParticleSystem::initShaders()
{
    mShaderBillboard
        = glow::Program::createFromFiles({"particles/billboard.gsh", "particles/billboard.vsh", "particles/billboard.fsh"});

    mShaderBillboardBlank
        = glow::Program::createFromFiles({"particles/billboard_blank.vsh", "particles/billboard_blank.fsh"});

    mShaderFeedback = glow::Program::createFromFiles({"particles/feedback.gsh", "particles/feedback.vsh"});
    mShaderFeedback->configureTransformFeedback({"gsPosition", "gsVelocity", "gsColor", "gsLifetime"}, GL_INTERLEAVED_ATTRIBS);

    mShaderEmitter
        = glow::Program::createFromFiles({"particles/emitter_feedback.gsh", "particles/emitter_feedback.vsh"});
    mShaderEmitter->configureTransformFeedback({"gsPosition", "gsVelocity", "gsColor", "gsLifetime"}, GL_INTERLEAVED_ATTRIBS);
}

void flock::ParticleSystem::initBuffers()
{
    std::vector<Particle> placeholderParticles(maxParticles);

    for (auto i = 0u; i < feedbackAmount; ++i)
    {
        mParticleBuffers[i] = glow::ArrayBuffer::create(Particle::Attributes(), placeholderParticles);
        mParticleFeedbacks[i] = glow::TransformFeedback::create(mParticleBuffers[i]);
        mParticleVaos[i] = glow::VertexArray::create(mParticleBuffers[i], GL_POINTS);
    }

    mEmitterBuffer = glow::ArrayBuffer::create(Emitter::Attributes(), mEmitters);
    mEmitterVao = glow::VertexArray::create(mEmitterBuffer, GL_POINTS);
}

void flock::ParticleSystem::initTextures()
{
    mTextureSpot = tex_utils::generateSpotTexture(particleTextureSize, particleTextureSize);
    mTextureRandom = tex_utils::generateRandom1DTexture(randomTextureSize);
}

void flock::ParticleSystem::performFeedbackDryrun()
{
    /**
     * This function
     * a) - Suppresses all errors related non-converging linkage of mShaderFeedback
     * b) - Fixes the strange behaviour of mShaderFeedback that usually requires a hotreload
     *
     * Not sure why exactly this is necessary
     */

    GLOW_SCOPED(enable, GL_RASTERIZER_DISCARD);

    for (auto i = 0; i < 2; ++i)
    {
        {
            auto const previousIndex = mState.ring.begin();
            auto const currentIndex = mState.ring.end();

            auto shader = mShaderFeedback->use();

            shader.setUniform("uParticleLifetime", mConfig.particleLifetime);
            shader.setUniform("uDeltaTime", mState.deltaTime);
            shader.setUniform("uTime", mState.time);
            shader.setUniform("uCTime", mState.time);
            shader.setTexture("uRandomTexture", mTextureRandom);

            mParticleVaos[previousIndex]->bind().negotiateBindings();

            {
                auto feedback = mParticleFeedbacks[currentIndex]->bind();

                feedback.begin(GL_POINTS);

                if (mState.ring.isFullCycle())
                {
                    // Process particles from last frame
                    mParticleVaos[previousIndex]->bind().drawTransformFeedback(mParticleFeedbacks[previousIndex]);
                }

                feedback.end();
            }
        }

        {
            auto const currentIndex = mState.ring.end();
            auto shader = mShaderBillboardBlank->use();
            mParticleVaos[currentIndex]->bind().drawTransformFeedback(mParticleFeedbacks[currentIndex]);
        }

        mState.ring.advance();
    }
}

void flock::ParticleSystem::init()
{
    initShaders();
    initBuffers();
    initTextures();

    performFeedbackDryrun();
}

void flock::ParticleSystem::update(float dt, entt::registry<uint32_t> &registry)
{
    // Update Time
    mState.time += dt;
    mState.deltaTime = dt;

    // Update Emitters
    mEmitters.clear();

    registry.view<EmitterC, TransformC>().each([&](auto const, EmitterC &emitter, TransformC &transform) {
        mEmitters.emplace_back(Emitter(transform.transform.position, emitter.velocity, emitter.color));
    });

    registry.view<StaticEmitterC>().each([&](auto const, StaticEmitterC &emitter) {
        if (emitter.active)
            mEmitters.emplace_back(Emitter(emitter.position, emitter.velocity, emitter.color));
    });
}

void flock::ParticleSystem::earlyRender()
{
    if (!mState.paused)
    {
        prepareEmitters();
        emitParticles();
    }
}

void flock::ParticleSystem::render(const glow::pipeline::RenderPipeline &pipeline,
                                   const glow::pipeline::RenderStage &stage,
                                   const glow::pipeline::CameraData &data)
{
    if (!mState.paused)
    {
        processParticles();
    }

    if (mState.ring.isFullCycle())
        renderParticles(pipeline, stage, data);

    if (!mState.paused)
        mState.ring.advance();
}
