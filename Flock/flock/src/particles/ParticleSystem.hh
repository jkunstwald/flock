#pragma once

#include <array>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_access.hpp>

#include <entt/entity/registry.hpp>

#include <glow/common/scoped_gl.hh>
#include <glow/fwd.hh>

#include <glow-extras/pipeline/stages/RenderStage.hh>
#include <glow-extras/pipeline/stages/StageCamera.hh>

#include "../util/Misc.hh"
#include "../util/Ring.hh"
#include "../util/TextureUtils.hh"
#include "Emitter.hh"

namespace flock
{
class ParticleSystem
{
private:
    struct Particle
    {
        glm::vec3 position = glm::vec3(0);
        glm::vec3 velocity = glm::vec3(0);
        glm::vec3 color = glm::vec3(1);
        float lifetime = 0.f;

        Particle() = default;
        Particle(glm::vec3 const& pos, glm::vec3 const& col = glm::vec3(0, 1, 0)) : position(pos), color(col) {}

        static std::vector<glow::ArrayBufferAttribute> Attributes()
        {
            return {
                {&Particle::position, "aPosition"},
                {&Particle::velocity, "aVelocity"},
                {&Particle::color, "aColor"},
                {&Particle::lifetime, "aLifetime"},
            };
        }
    };


private:
    // == Static Config ==
    static constexpr auto feedbackQueueLength = 2;
    static constexpr auto emittersIndex = 2;
    static constexpr auto feedbackAmount = feedbackQueueLength + 1;

    static constexpr auto maxParticles = 1024 * 1024;
    static constexpr auto particleTextureSize = 256;
    static constexpr auto randomTextureSize = 2048;

    // == Buffers ==
    std::array<glow::SharedArrayBuffer, feedbackAmount> mParticleBuffers;
    std::array<glow::SharedTransformFeedback, feedbackAmount> mParticleFeedbacks;
    std::array<glow::SharedVertexArray, feedbackAmount> mParticleVaos;

    std::vector<Emitter> mEmitters{};
    glow::SharedArrayBuffer mEmitterBuffer;
    glow::SharedVertexArray mEmitterVao;

    // == Shaders ==
    glow::SharedProgram mShaderFeedback;
    glow::SharedProgram mShaderEmitter;
    glow::SharedProgram mShaderBillboard;
    glow::SharedProgram mShaderBillboardBlank;

    // == Textures ==
    glow::SharedTexture2D mTextureSpot;
    glow::SharedTexture2D mTextureRandom;

    // == Config and State ==
public: // public for gui tweaking
    struct Config
    {
        float particleLifetime = 2.5f;
        float emitPeriod = .5f / 1000.f; // .5ms (This should be lower than the lowest possible frametime)
        int emitCount = 1;               // emitCount per emitPeriod
        float billboardSize = 0.1f;      //.25f;
        float velocityScale = 0.35f;     // 1.f;
    } mConfig{};

private:
    struct State
    {
        bool systemEmpty = true;
        bool paused = false;
        float time = 0.f;
        float deltaTime = 0.f;
        Ring<feedbackQueueLength> ring{};
    } mState{};

private:
    void renderParticles(glow::pipeline::RenderPipeline const& pipeline,
                         glow::pipeline::RenderStage const& stage,
                         glow::pipeline::CameraData const& data);

    void prepareEmitters();
    void emitParticles();
    void processParticles();

    void initShaders();
    void initBuffers();
    void initTextures();

    void performFeedbackDryrun();

public:
    void init();

    void update(float dt, entt::registry<uint32_t>& registry);

    /// This should get called as early as possible into the frame for GPU-stall reasons
    /// Tested 0.1ms faster on a 2080 for a small demo scene when called as the first API call vs right before render
    void earlyRender();

    void render(glow::pipeline::RenderPipeline const& pipeline,
                glow::pipeline::RenderStage const& stage,
                glow::pipeline::CameraData const& data);
};

}
