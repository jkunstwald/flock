#include "PrimitiveParticleRenderer.hh"

#include <iostream>

#include <glm/ext.hpp>

#include <glow/common/scoped_gl.hh>

#include <glow-extras/geometry/Cube.hh>
#include <glow-extras/geometry/Line.hh>
#include <glow-extras/geometry/Quad.hh>
#include <glow-extras/geometry/UVSphere.hh>

flock::PrimitiveParticleRenderer::PrimitiveParticleRenderer(entt::registry<uint32_t>* reg) : mRegistry(reg)
{
    mQuad = glow::geometry::Quad<PrimitiveVertex>().generate();
    mCube = glow::geometry::Cube<PrimitiveVertex>().generate();
    mLine = glow::geometry::Line<PrimitiveVertex>().generate();
    mSphere = glow::geometry::UVSphere<PrimitiveVertex>().generate();

    mShaderTransparent = glow::Program::createFromFiles({"primitives/primitive.vsh", "primitives/primitive.fsh"});
}

void flock::PrimitiveParticleRenderer::render(glow::pipeline::RenderPipeline const& pipeline,
                                              glow::pipeline::RenderStage const& stage,
                                              glow::pipeline::CameraData const& data)
{
    auto shader = mShaderTransparent->use();
    stage.prepareShader(pipeline, mShaderTransparent, shader);
    shader.setUniform("uVP", data.vp);

    // Lines
    {
        GLOW_SCOPED(enable, GL_CONSERVATIVE_RASTERIZATION_NV);

        auto boundLine = mLine->bind();

        mRegistry->view<LineParticleC>().each([&](auto const, LineParticleC& line) {
            if (line.active)
            {
                shader.setUniform("uColor", line.color);
                shader.setUniform("uModel", line.modelMatrix);

                boundLine.draw();
            }
        });
    }

    // Spheres
    {
        auto boundSphere = mSphere->bind();

        mRegistry->view<SphereParticleC>().each([&](auto const, SphereParticleC& sphere) {
            if (sphere.active)
            {
                sphere.tick(mDeltaTime);

                shader.setUniform("uColor", sphere.color);
                shader.setUniform("uModel", sphere.modelMatrix);

                boundSphere.draw();
            }
        });
    }
}

void flock::PrimitiveParticleRenderer::update(float dt)
{
    mDeltaTime = dt;
}

void flock::LineParticleC::setModelMatrix(const glm::vec3& start, const glm::vec3& end)
{
    modelMatrix = translate(start) * glm::mat4(glm::mat3(end - start, glm::vec3(0, 1, 0), glm::vec3(0, 0, 1)));
}

flock::LineParticleC::LineParticleC(const glm::vec3& start, const glm::vec3& end, const glm::vec4& color) : color(color)
{
    setModelMatrix(start, end);
}

void flock::SphereParticleC::setModelMatrix(const glm::vec3& pos, float radius)
{
    modelMatrix = translate(pos) * scale(glm::vec3(radius));
}

void flock::SphereParticleC::tick(float dt)
{
    time += dt;
    color.a = 1.f - std::min(time / duration, 1.f);
}

flock::SphereParticleC::SphereParticleC(const glm::vec3& pos, float radius, const glm::vec4& color, float duration)
  : color(color), duration(duration)
{
    setModelMatrix(pos, radius);
}
