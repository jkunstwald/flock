#pragma once

#include <vector>

#include <entt/entity/registry.hpp>

#include <glow/common/property.hh>
#include <glow/common/shared.hh>
#include <glow/objects/ArrayBufferAttribute.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/VertexArray.hh>

#include <glow-extras/pipeline/stages/RenderStage.hh>
#include <glow-extras/pipeline/stages/StageCamera.hh>

namespace flock
{
struct LineParticleC
{
    glm::mat4 modelMatrix;
    glm::vec4 color;
    bool active = true;

    void setModelMatrix(glm::vec3 const& start, glm::vec3 const& end);
    LineParticleC(glm::vec3 const& start, glm::vec3 const& end, glm::vec4 const& color);
};

struct SphereParticleC
{
    glm::mat4 modelMatrix;
    glm::vec4 color;
    bool active = true;
    float duration;
    float time = 0.f;

    void setModelMatrix(glm::vec3 const& pos, float radius);
    void tick(float dt);
    SphereParticleC(glm::vec3 const& pos, float radius, glm::vec4 const& color, float duration);
};

class PrimitiveParticleRenderer
{
private:
    struct PrimitiveVertex
    {
        glm::vec3 pos{0};

        PrimitiveVertex() = default;
        PrimitiveVertex(float u, float v) : pos(u, v, 0.0) {}
        PrimitiveVertex(glm::vec3 pos) : pos(pos) {}
        PrimitiveVertex(glm::vec3 position, glm::vec3, glm::vec3, glm::vec2) : pos(position) {}
        static std::vector<glow::ArrayBufferAttribute> attributes()
        {
            return {
                {&PrimitiveVertex::pos, "aPosition"}, //
            };
        }
    };

    entt::registry<uint32_t>* const mRegistry;

    glow::SharedVertexArray mQuad;
    glow::SharedVertexArray mCube;
    glow::SharedVertexArray mLine;
    glow::SharedVertexArray mSphere;
    glow::SharedProgram mShaderTransparent;

    float mDeltaTime = 0.f;

public:
    PrimitiveParticleRenderer(entt::registry<uint32_t>* reg);

    /// Renders all stored primitives
    void render(glow::pipeline::RenderPipeline const& pipeline,
                glow::pipeline::RenderStage const& stage,
                glow::pipeline::CameraData const& data);

    void update(float dt);
};
}
