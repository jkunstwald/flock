#include "Swarm.hh"

#ifdef _MSC_VER

#include <algorithm>
#include <execution>

#define FLOCK_EXEC_POLICY_PAR_UNSEQ std::execution::par_unseq

#else

// Non-MSVC compilers did not yet implement C++17 execution policies
#include <pstl/algorithm>
#include <pstl/execution>

#define FLOCK_EXEC_POLICY_PAR_UNSEQ pstl::execution::par_unseq

#endif

#include "../util/Misc.hh"

glm::ivec3 flock::Swarm::getVoxelIndex(const glm::vec3& pos) const
{
    return glm::ivec3(static_cast<int>(pos.x / mPerceptionRadius), static_cast<int>(pos.y / mPerceptionRadius),
                      static_cast<int>(pos.z / mPerceptionRadius));
}

float flock::Swarm::transformDistance(float distance, flock::Swarm::EDistanceType type)
{
    switch (type)
    {
    case EDistanceType::Linear:
        return distance;
    case EDistanceType::InverseLinear:
        return distance == 0.f ? 0.f : 1.f / distance;
    case EDistanceType::Quadratic:
        return std::pow(distance, 2.f);
    case EDistanceType::InverseQuadratic:
    {
        float quad = std::pow(distance, 2);
        return quad == 0.f ? 0.f : 1.f / quad;
    }
    default:
        return 0.f;
    }
}

glm::vec3 flock::Swarm::clampVectorLength(const glm::vec3& input, float maxLength)
{
    auto const inputLength = glm::length(input);
    if (inputLength > maxLength)
        return input * (maxLength / inputLength);
    else
        return input;
}

void flock::Swarm::checkVoxelForBoids(uint32_t id,
                                      flock::SwarmlingC& swarmling,
                                      std::vector<flock::Swarm::NearbySwarmling>& outVector,
                                      const glm::uvec3& voxelIndex) const noexcept
{
    auto iter = mVoxelCache.find(voxelIndex);
    if (iter != mVoxelCache.end())
    {
        for (auto& nearby : iter->second)
        {
            if (id == nearby.id)
                continue;

            auto const& p1 = swarmling.position;
            auto const& p2 = nearby.position;
            auto distanceVec = p2 - p1;
            float distance = glm::length(distanceVec);

            float blindAngle = glm::degrees(glm::angle(swarmling.velocity * -1, distanceVec));
            if (distance <= mPerceptionRadius && (mBlindspotAngleDeg <= blindAngle || glm::length(swarmling.velocity) == 0.f))
            {
                outVector.emplace_back(NearbySwarmling{nearby.id, distanceVec, distance});
            }
        }
    }
}

std::vector<flock::Swarm::NearbySwarmling> flock::Swarm::getNearbySwarmlings(uint32_t id, flock::SwarmlingC& swarmling) const noexcept
{
    std::vector<NearbySwarmling> result;
    result.reserve(500); // TODO: Number is arbitrary
    auto const baseVoxelIndex = getVoxelIndex(swarmling.position) - glm::ivec3(1);

    for (int const x : {0, 1, 2})
        for (int const y : {0, 1, 2})
            for (int const z : {0, 1, 2})
                checkVoxelForBoids(id, swarmling, result, baseVoxelIndex + glm::ivec3(x, y, z));

    return result;
}

void flock::Swarm::updateSwarmling(uint32_t id, flock::SwarmlingC& swarmling) const noexcept
{
    glm::vec3 separationSum{0};
    glm::vec3 headingSum{0};
    glm::vec3 positionSum{0};

    auto const nearby = getNearbySwarmlings(id, swarmling);

    {
        for (NearbySwarmling const& closeSwarmling : nearby)
        {
            if (closeSwarmling.distance == 0.f)
            {
                // Randomly evade
                static thread_local std::mt19937 randomGen;
                separationSum += util::getRandomDirection(randomGen) * 100.f;
            }
            else if (closeSwarmling.distance < mSeparationRadius)
            {
                separationSum -= closeSwarmling.direction;
            }

            auto const& closeSwarmlingC = mRegistry->get<SwarmlingC>(closeSwarmling.entityId);

            headingSum += closeSwarmlingC.velocity;
            positionSum += closeSwarmlingC.position;
        }
    }

    auto steeringTarget = swarmling.position;
    float targetDistance = -1.f;
    for (auto const& target : mSteeringTargets)
    {
        float distance = transformDistance(glm::distance(target, swarmling.position), mSteeringTargetType);
        if (targetDistance < 0 || distance < targetDistance)
        {
            steeringTarget = target;
            targetDistance = distance;
        }
    }

    // Separation: steer to avoid crowding local flockmates
    auto separation = separationSum;

    // Alignment: steer towards the average heading of local flockmates
    auto alignment = nearby.size() > 0 ? headingSum / nearby.size() : headingSum;

    // Cohesion: steer to move toward the average position of local flockmates
    auto avgPosition = nearby.size() > 0 ? positionSum / nearby.size() : swarmling.position;
    auto cohesion = avgPosition - swarmling.position;

    // Steering: steer towards the nearest target location (like a moth to the light)
    auto steering = util::safeNormalize(steeringTarget - swarmling.position) * targetDistance;

    // calculate boid acceleration
    glm::vec3 acceleration;
    acceleration += separation * mSeparationWeight;
    acceleration += alignment * mAlignmentWeight;
    acceleration += cohesion * mCohesionWeight;
    acceleration += steering * mSteeringWeight;
    swarmling.acceleration = clampVectorLength(acceleration * 0.5f + swarmling.acceleration * 0.5f, mMaxAcceleration);
}

flock::Swarm::Swarm(entt::registry<uint32_t>* reg) : mRegistry(reg) {}

void flock::Swarm::tick(float dt)
{
    // Rebuild Swarmling Targets
    {
        mSteeringTargets.clear();
        mRegistry->view<TransformC, SwarmlingTargetC>().each([&](auto const, TransformC& transform, SwarmlingTargetC&) {
            mSteeringTargets.push_back(transform.transform.position);
        });
    }

    // Rebuild Voxel Cache
    {
        mVoxelCache.clear();

        auto view = mRegistry->view<TransformC, SwarmlingC>();
        std::for_each(view.begin(), view.end(), [&](auto const ent) {
            auto& swarmling = mRegistry->get<SwarmlingC>(ent);
            mVoxelCache[getVoxelIndex(swarmling.position)].emplace_back(
                VoxelCacheSwarmling{ent, swarmling.position, swarmling.velocity});
        });
    }


    // Update Boids
    {
        // NOTE: std::execution::parallel_unsequenced_policy
        // Only call thread safe methods here
        auto view = mRegistry->view<TransformC, SwarmlingC>();
        std::for_each(FLOCK_EXEC_POLICY_PAR_UNSEQ, view.begin(), view.end(),
                      [&](auto const ent) { updateSwarmling(ent, mRegistry->get<SwarmlingC>(ent)); });
    }


    // Integrate
    {
        // NOTE: std::execution::parallel_unsequenced_policy
        // Only call thread safe methods here
        auto view = mRegistry->view<TransformC, SwarmlingC>();
        std::for_each(FLOCK_EXEC_POLICY_PAR_UNSEQ, view.begin(), view.end(), [&](auto const ent) {
            auto& swarmling = mRegistry->get<SwarmlingC>(ent);
            auto& transform = mRegistry->get<TransformC>(ent);
            swarmling.velocity = clampVectorLength(swarmling.velocity + swarmling.acceleration * dt, mMaxVelocity);
            swarmling.position += swarmling.velocity * dt;
            transform.transform.position = swarmling.position;
            // TODO: Why has X to be inverted?
            transform.transform.lerpToRotation(glow::transform::RotationFromDirection(glm::vec3(
                                                   -swarmling.velocity.x, swarmling.velocity.y, swarmling.velocity.z)),
                                               dt * glm::length(swarmling.velocity));
        });
    }
}
