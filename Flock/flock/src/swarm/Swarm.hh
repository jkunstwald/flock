#pragma once

#include <atomic>
#include <iostream>
#include <random>
#include <unordered_map>
#include <vector>

#include <aion/Tracer.hh>

#include <glm/vec3.hpp>

#include <entt/entity/registry.hpp>

#include "../logic/TransformC.hh"
#include "SwarmlingC.hh"

namespace flock
{
class Swarm
{
public:
    enum class EDistanceType
    {
        Linear,
        InverseLinear,
        Quadratic,
        InverseQuadratic
    };

private:
    struct NearbySwarmling
    {
        uint32_t const entityId;
        glm::vec3 const direction;
        float const distance;
    };

    struct VoxelCacheSwarmling
    {
        uint32_t const id;
        glm::vec3 const position;
        glm::vec3 const velocity;
    };

    struct Vec3Hasher
    {
        size_t operator()(glm::ivec3 const& v) const
        {
            size_t const h1(std::hash<int>()(v.x));
            size_t const h2(std::hash<int>()(v.y));
            size_t const h3(std::hash<int>()(v.z));
            return (h1 * 31 + h2) * 31 + h3;
        }
    };

public:
    float mPerceptionRadius = 10.f; // Originally 20
    float mSeparationRadius = 3.5f;

    float mSeparationWeight = 5.f;
    float mAlignmentWeight = 0.125f;
    float mCohesionWeight = 0.01f; // Originally .01
    float mSteeringWeight = 0.4f;  // Originally .5

    float mBlindspotAngleDeg = 20.f;

    float mMaxAcceleration = 35.f; // Originally 25
    float mMaxVelocity = 40.f;     // Originally 20

    EDistanceType mSteeringTargetType = EDistanceType::Linear;

private:
    std::vector<glm::vec3> mSteeringTargets = {};

    std::unordered_map<glm::ivec3, std::vector<VoxelCacheSwarmling>, Vec3Hasher> mVoxelCache{};
    entt::registry<uint32_t>* const mRegistry;

private:
    /// Returns a voxel index for a position
    inline glm::ivec3 getVoxelIndex(glm::vec3 const& pos) const;

    static inline float transformDistance(float distance, EDistanceType type);
    static inline glm::vec3 clampVectorLength(glm::vec3 const& input, float maxLength);

    void checkVoxelForBoids(uint32_t id, SwarmlingC& swarmling, std::vector<NearbySwarmling>& outVector, glm::uvec3 const& voxelIndex) const
        noexcept;
    std::vector<NearbySwarmling> getNearbySwarmlings(uint32_t id, SwarmlingC& swarmling) const noexcept;
    void updateSwarmling(uint32_t id, SwarmlingC& swarmling) const noexcept;

public:
    Swarm(entt::registry<uint32_t>* reg);

    void tick(float dt);
};
}
