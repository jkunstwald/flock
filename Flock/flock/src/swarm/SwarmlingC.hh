#pragma once

#include <glm/vec3.hpp>

namespace flock
{
struct SwarmlingC
{
    glm::vec3 position = glm::vec3(0);
    glm::vec3 velocity = glm::vec3(0);
    glm::vec3 acceleration = glm::vec3(0);
};

struct SwarmlingTargetC
{
};
}
