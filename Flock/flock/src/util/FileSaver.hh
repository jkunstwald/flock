#pragma once

#include <fstream>
#include <iostream>
#include <string>

namespace flock
{
struct SaveFile
{
    float highscore = 0;
};

class FileSaver
{
private:
    static constexpr auto savefilePathBin = "flock_save.dat";

public:
    static void storeSavegame(SaveFile const& data)
    {
        std::ofstream savefile;
        savefile.open(savefilePathBin, std::ios::out | std::ios::binary);

        if (savefile.is_open())
        {
            savefile.write(reinterpret_cast<const char*>(&data.highscore), sizeof(float));
            savefile.close();
        }
        else
        {
            std::cerr << "[SaveFile] Unable to write savegame " << savefilePathBin << std::endl;
        }
    }

    static SaveFile loadSavegame()
    {
        SaveFile res;
        std::ifstream savefile(savefilePathBin, std::ios::binary);
        if (savefile.is_open())
        {
            if (savefile.read(reinterpret_cast<char*>(&res.highscore), sizeof(float)))
            {
                std::cout << "[SaveFile] Savegame loaded" << std::endl;
            }
            else
            {
                std::cout << "[SaveFile] Savegame corrupt" << std::endl;

                // Override with an empty savefile
                res.highscore = 0.f;
                storeSavegame(SaveFile{});
            }

            savefile.close();
        }
        else
        {
            std::cout << "[SaveFile] No savegame found" << std::endl;
        }

        return res;
    }
};
}
