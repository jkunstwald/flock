#pragma once

#include <algorithm>
#include <cmath>
#include <random>

#include <glm/ext.hpp>
#include <glm/glm.hpp>

namespace flock
{
namespace util
{
#define FLOCK_PI 3.14159265358979323846f

constexpr inline unsigned packColor(glm::vec3 const& color) noexcept
{
    return static_cast<unsigned>(color.r) | (static_cast<unsigned>(color.g) << 8) | (static_cast<unsigned>(color.b) << 16);
}

/// Hermite interpolation, equivalent to glsl smoothstep()
constexpr inline float smoothstep(float edge0, float edge1, float x) noexcept
{
    float const t = std::clamp((x - edge0) / (edge1 - edge0), 0.f, 1.f);
    return t * t * (3.f - 2.f * t);
}

/// Returns a random direction vector, normalized
inline glm::vec3 getRandomDirection(std::mt19937& rng) noexcept
{
    static std::uniform_real_distribution<float> thetaRange(0.0f, 6.28318530717f);
    static std::uniform_real_distribution<float> oneRange(0, 1);

    float const theta = thetaRange(rng);
    float const r = sqrt(oneRange(rng));
    float const z = sqrt(1.0f - r * r) * (oneRange(rng) > 0.5f ? -1.0f : 1.0f);
    return glm::vec3(r * cos(theta), r * sin(theta), z);
}

inline glm::vec2 getRandomDirection2D(std::mt19937& rng) noexcept
{
    static std::uniform_real_distribution<float> oneRange(0, 1);

    auto const rads = oneRange(rng) * FLOCK_PI * 2;
    return glm::vec2(cos(rads), sin(rads));
}

/// Normalize that does not crash when given a zero vector
inline glm::vec3 safeNormalize(glm::vec3 const& v) noexcept
{
    auto const lengthSq = glm::length2(v);
    if (lengthSq == 0.f)
        return v;
    else
    {
        return v * (1 / std::sqrt(lengthSq));
    }
}

inline glm::vec3 safeNormalize(glm::vec3 const& v, float lengthSq) noexcept
{
    if (lengthSq == 0.f)
        return v;
    else
    {
        return v * (1 / std::sqrt(lengthSq));
    }
}

/// Smoothed lerp, framerate-correct
template <typename T>
inline T smoothLerp(T a, T b, float smoothing, float dt) noexcept
{
    return glm::lerp(a, b, 1 - std::pow(smoothing, dt));
}

/// Exponential decay, framerate-correct damp / lerp
template <typename T>
inline T exponentialDecayLerp(T a, T b, float lambda, float dt) noexcept
{
    return glm::lerp(a, b, 1 - std::exp(-lambda * dt));
}
}
}
