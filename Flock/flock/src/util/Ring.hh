#pragma once

#include <cstdint>

namespace flock
{
template <int N>
struct Ring
{
private:
    uint32_t mPosition = 0;
    bool mFullCircle = false;

public:
    Ring() = default;

    void advance()
    {
        mFullCircle |= mPosition == N - 2;
        mPosition = (mPosition + 1) % N;
    }

    [[nodiscard]] constexpr uint32_t begin() const noexcept { return mPosition; }
    [[nodiscard]] constexpr uint32_t end() const noexcept { return (mPosition + N - 1) % N; }
    [[nodiscard]] constexpr bool isFullCycle() const noexcept { return mFullCircle; }
};
}
