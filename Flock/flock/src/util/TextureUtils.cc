#include "TextureUtils.hh"

#include <random>

#include <glow/common/scoped_gl.hh>

glow::SharedTexture2D flock::tex_utils::generateRandom1DTexture(int extent)
{
    auto tex = glow::Texture2D::create(extent, 1, GL_RGBA);

    unsigned const size = static_cast<unsigned>(extent) * 4u;

    std::vector<float> randomData(size);

    for (auto n = 0u; n < size; ++n)
        randomData[n] = rand() / static_cast<float>(RAND_MAX);

    {
        auto boundTex = tex->bind();
        boundTex.setWrap(GL_REPEAT, GL_REPEAT);
        boundTex.setMinFilter(GL_LINEAR);
        boundTex.setMagFilter(GL_LINEAR);
        boundTex.setData(GL_RGBA, extent, 1, GL_RGBA, GL_FLOAT, randomData.data());
    }

    return tex;
}

glow::SharedTexture2D flock::tex_utils::generateSpotTexture(unsigned width, unsigned height)
{
    auto tex = glow::Texture2D::create(static_cast<int>(width), static_cast<int>(height), GL_RED);

    unsigned const size = width * height;

    std::vector<uint8_t> spotData(size);

    const float fMaxDist = std::sqrt(size / 2.f);
    for (auto y = 0u; y < height; ++y)
        for (auto x = 0u; x < width; ++x)
        {
            float length = std::sqrt((x - width / 2.f) * (x - width / 2.f) + (y - height / 2.f) * (y - height / 2.f));

            float fSpot = std::pow((fMaxDist - length) / fMaxDist, 8.f);
            fSpot = fSpot > 1.f ? 1.f : fSpot;

            spotData[x + y * width] = fSpot > 0 ? static_cast<uint8_t>(fSpot * 255.f) : 0;
        }

    {
        auto boundTex = tex->bind();
        boundTex.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
        boundTex.setMagFilter(GL_LINEAR);
        boundTex.setMinFilter(GL_LINEAR_MIPMAP_LINEAR);
        boundTex.setData(GL_RED, static_cast<int>(width), static_cast<int>(height), GL_RED, GL_UNSIGNED_BYTE, spotData.data());
        boundTex.generateMipmaps();
    }

    return tex;
}
