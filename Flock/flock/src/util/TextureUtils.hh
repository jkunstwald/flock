#pragma once

#include <cstdint>

#include <glow/objects/Texture2D.hh>

namespace flock
{
namespace tex_utils
{
glow::SharedTexture2D generateRandom1DTexture(int extent);
glow::SharedTexture2D generateSpotTexture(unsigned width, unsigned height);
}
}
