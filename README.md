# Flock

![hero](https://graphics.rwth-aachen.de:9000/jkunstwald/flock/raw/master/other/readme_hero_cropped.gif)

A tech-demo sized game about a plane defending itself from an unending swarm of monkey heads.

## Tech

- `glow-extras-pipeline` based rendering
- 3D boid simulation with voxel caching
- GPU-calculated billboard particles using Transform Feedback
- Barebones [qu3e](https://github.com/RandyGaul/qu3e) physics engine wrapper for raycasts and collision detection

## Game

- Kill the spawner spheres to hold back the swarm of monkey boids
- Red boss spawners are tougher, and unleash a steady stream of boids
- Collect *green spheres* to upgrade your weapon
- Green spheres home in on the ship while not firing
- Boost while not firing to dodge boids
- Your ship is slower when flying backwards

## Controls

| Input | Action |
|--|--|
| `WASD` | Move horizontally |
| Mouse | Aim (Rotate and move vertically) |
| `Left Click` (Hold)| Fire |
| `QE` | Move vertically |
| `Left Shift` | Boost |
| `R` | Respawn after death |
| `F11` | Fullscreen |
| `Tab` | Config mode |
| `Esc` | Quit |

## Building

If you are not using MSVC, Flock requires [Intel TBB](https://github.com/01org/tbb) to be installed, as a drop-in replacement for C++17 execution policies. It is searched for by CMake. All other dependencies are included as git submodules.

Was tested to compile with clang 7.0.0-3 and MSVC 15.
